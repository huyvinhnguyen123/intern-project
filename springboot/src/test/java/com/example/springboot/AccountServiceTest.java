package com.example.springboot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import com.example.springboot.entity.account.Account;
import com.example.springboot.entity.account.AccountDTO;
import com.example.springboot.entity.account.AccountRepository;
import com.example.springboot.entity.account.AccountService;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    
    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void getAccounts() {
        // Create some mock accounts to be returned by the repository
        List<Account> accounts = new ArrayList<Account>();

        Account account1 = new Account();
        account1.setEmail("dev1@gmail.com");
        account1.setPassword("dev1");
        Account account2 = new Account();
        account2.setEmail("dev2@gmail.com");
        account2.setPassword("dev2");
        Account account3 = new Account();
        account3.setEmail("dev3@gmail.com");
        account3.setPassword("dev3");

        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        
        // Set up the mock repository to return the accounts
        when(accountRepository.findAll()).thenReturn(accounts);

        // Call the get all accounts method on the service
        List<Account> retrievedAccounts = accountService.getAllAccounts();
        
        // Verify that the repository was called once to retrieve accounts
        verify(accountRepository, times(1)).findAll();

        // Assert that the expected accounts are returned
        assertEquals(accounts, retrievedAccounts);
    }

    @Test
    public void getAccountById() {
        // Create mock data
        Account account = new Account();
        account.setEmail("huyanphat113@gmail.com");
        account.setPassword("Lumina");

        Optional<String> optionalAccount = Optional.ofNullable(account.getAccountId()); 

        // Save the account to the repository
        // accountRepository.save(account);

        // Set up a mock repository that expects the user to be saved
        when(accountRepository.findByIdAndReturnId(account.getAccountId())).thenReturn(optionalAccount);

        // Call the add user method on the service
        AccountDTO accountDTO =  accountService.getAccountById(account.getAccountId());

        // Verify that the repository was called once to retrieve account
        verify(accountRepository).findByIdAndReturnId(account.getAccountId());

        // Assert that the expected account are returned
        assertEquals(account, accountDTO);
    }

    // @Test
    // public void getAccountByEmail() {
    //     // Create mock data
    //     Account account = new Account();
    //     account.setEmail("huyanphat113@gmail.com");
    //     account.setPassword("Lumina");

    //     // Set up a mock repository that expect find account by email
    //     when(accountRepository.findByEmail(account.getEmail())).thenReturn(Optional.of(account.getEmail()));
        
    //     // Call the method from service
    //     String accountEmail = accountService.getAccountByEmail(account.getEmail());

    //     // Verify that repository was called once to retrieve account
    //     verify(accountRepository).findByEmail(account.getEmail());

    //     // Assert that the expected account's email are returned
    //     assertEquals(account.getEmail(), accountEmail);
    // }

    @Test
    public void addAccount() {
        // Create mock data
        Account account = new Account();
        account.setEmail("huyanphat113@gmail.com");
        account.setPassword("Lumina");

        // Set up a moSck repository that expects the user to be saved
        when(accountRepository.save(account)).thenReturn(account);

        // Call the add user method on the controller, service
        Account addedUser = accountService.createUserAccount(account);

        // Verify that the user was saved to the repository
        verify(accountRepository).save(account);

        // Verify that the added user matches the original user
        assertEquals(account, addedUser);
    }

    // @Test
    // public void resetPassword() {
    //     Account account = new Account();
    //     account.setEmail("huyanphat113@gmail.com");
    //     account.setPassword("Lumina");

    //     // when(accountRepository.save(account)).thenReturn(account);

    //     when(accountRepository.findByEmail(account.getEmail())).thenReturn(Optional.of(account));

    //     Account updateAccount = new Account();
    //     updateAccount.setPassword("123");
    //     updateAccount.setRetypePassword("123");
    //     boolean result = accountService.resetPassword(account.getEmail(), updateAccount);

    //     // verify(accountRepository).save(account);
        
    //     verify(accountRepository).findByEmail(account.getEmail());

    //     assertTrue(result);
    //     assertEquals("123", account.getPassword());
    //     assertEquals("123", account.getRetypePassword());
    // }

    @Test
    public void deleteAccount() {
        Account account = new Account();
        account.setEmail("huyanphat113@gmail.com");
        account.setPassword("Lumina");

        when(accountRepository.findById(account.getAccountId())).thenReturn(Optional.of(account));
        
        accountService.deleteAccount(account.getAccountId());

        verify(accountRepository).deleteById(account.getAccountId());
    }
}
