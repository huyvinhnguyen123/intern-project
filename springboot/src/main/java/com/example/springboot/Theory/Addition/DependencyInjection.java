package com.example.springboot.theory.Addition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/* 
 ^ What is Dependency Injection?

 * Before we go to defind what is Dependency Injection?
 * Let's look at the IOC
 
 ^ What is IOC?

 * IoC is a design principle which recommends the inversion of different kinds of controls in object-oriented design 
 * to achieve loose coupling between application classes
 
 * => Easy ways to explain Inversion of Control (IoC) is a design pattern where the control of the application is inverted. 
 * Instead of your application controlling the creation and flow of objects, a framework or container controls it. 
 * The container is responsible for managing the dependencies and the lifecycle of the objects in your application. 
 * This means that instead of you having to manually create objects and pass them around, the container does it for you. 
 * This allows for a more flexible and modular application design. 
  
 ^ What is Tight Coupling and Loose Coupling?

 * Dependency Injection (DI) is a design pattern in which an object is provided with its dependencies rather than creating them itself.
 * => Easy ways to explain dependency injection is a design pattern in which an object or function receives other objects or functions that it depends on.
   
 * Spring Boot is a popular Java framework that uses DI extensively to manage dependencies and facilitate loose coupling between components.

 * Tight coupling occurs when a class is directly dependent on the concrete implementation of another class, 
 * making it difficult to change or maintain the code in the future. 
 * Loose coupling, on the other hand, occurs when a class is dependent on an interface or abstract class, 
 * rather than the concrete implementation, making it easier to swap out implementations without affecting the rest of the code.

 * Basically Tight Coupling means one class is dependent on another class. 
 * Loose Coupling means one class is dependent on interface rather than class.
*/

/* 
 & Tight Coupling Example:
*/
class Item {
    public void add() {
        //add item to cart
    }
}

class ShoppingCart {
    private Item item;

    public ShoppingCart() {
        item = new Item();
    }

    public void addItemToCart() {
        item.add();
    }
}

/* 
 * In this example, the ShoppingCart class is tightly coupled with the Item class. 
 * The ShoppingCart class creates an instance of the Item class using the new operator in its constructor, 
 * and the addItemToCart() method calls the add() method on the Item instance. 
 * This means that any changes to the Item class will require changes to the ShoppingCart class as well, 
 * making the code difficult to maintain.
*/

/* 
 & Loose Coupling Example
*/

interface ShoppingCartItem2 {
    public void add();
}

class ShoppingCart2 {
    private ShoppingCartItem2 item;

    public ShoppingCart2(ShoppingCartItem2 item) {
        this.item = item;
    }

    public void addItemToCart() {
        item.add();
    }
}

class Item2 implements ShoppingCartItem2 {
    public void add() {
        //add item to cart
    }
}

/*
 * In this example, the ShoppingCart class is loosely coupled with the Item class 
 * by depending on the ShoppingCartItem interface rather than the Item class directly. 
 * The ShoppingCart class takes an instance of the ShoppingCartItem interface in its constructor, 
 * and the addItemToCart() method calls the add() method on the ShoppingCartItem instance. 
 * This means that any changes to the Item class that implement the ShoppingCartItem 
 * interface will not require changes to the ShoppingCart class, making the code more flexible and easier to maintain.
 */

/* 
 ^ Some useful annotation to inject 

 ? @Autowired
 * this annotation used to automatically inject dependent objects into a Spring bean. 
 * It can be used to inject dependencies on variables, constructors, and setters. 
 * The Spring Framework will scan your application context for classes that use the @Autowired annotation 
 * and automatically inject them with the matching beans. 
 * It is commonly used to inject dependencies such as DAOs, services, repositories, 
 * and other Spring-managed components into your application classes
 
 ? @Primary
 * The @Primary annotation in Spring is used to specify a primary bean when there are multiple beans of the same type. 
 * This annotation is used in conjunction with @Autowired to specify which bean should be injected 
 * when there are multiple beans of the same type in the application context.
 
 ? @Qualified
 * The @Qualified annotation in Spring is used in conjunction with the @Autowired annotation 
 * to specify which bean should be wired when multiple beans of the same type are present in the context.

 ? @RequiredArgsConstructor
 * generate constructor with all final fields as arguments.
 * Generally we use this annotation to replace @Autowired.
 * Because We use lombok.
 
 ? @Bean
 * @Bean is an annotation used to indicate that a method produces a bean that should be managed by the Spring container. 
 * This annotation is often used in Java-based configuration to declare a bean directly in a configuration class.

 ? @Component
 * @Component annotation is used to indicate that a class is a component or a bean 
 * that should be automatically detected and instantiated by a Spring container. 
 * It is a generic stereotype annotation for any Spring-managed component.
 
 * When the Spring container detects a class with the @Component annotation, 
 * it will create an instance of the class and register it as a bean in the container, 
 * which can then be injected into other classes or components that depend on it.
 
 * @Component is a core Spring framework annotation and is widely used in Spring-based applications 
 * to enable dependency injection and manage object lifecycles. 
 
 ? @Service
 * @Service is an annotation that marks a class as a service provider. 
 * It is typically used in the service layer of a Spring application to define a service that carries out some business logic. 
 * When a class is annotated with @Service, Spring creates an instance of the class as a bean in the application context, 
 * making it available for dependency injection.

 ? @Repository
 * @Repository is a Spring annotation used to indicate that a class is a repository, 
 * which is an abstraction layer between the application and the persistence layer (usually a database).
 * It is used to handle database operations like inserting, updating, and deleting data. 
 * The @Repository annotation is used in combination with the @Autowired annotation to inject a repository bean into a service or controller class.

 ? @Controller
 * @Controller is used to define a controller class that returns a view. 
 * It is typically used in a Spring MVC web application and returns the response to the client in the form of a view.

 ? @RestController
 * @RestController, on the other hand, is used to define a controller class that returns data in the form of JSON or XML. 
 * It is typically used in a RESTful web service application and returns the response to the client in the form of JSON or XML.

 * Both @Controller and @RestController are responsible for processing the incoming requests, 
 * handling the business logic and returning the response to the client. 
 * The difference is that @RestController combines @Controller and @ResponseBody annotations, 
 * so it automatically serializes the response to JSON or XML format.
*/

/* 
 & This is example with case using @Primary
*/
/* 
    ? @Component
    ? @Primary
    * class FooImpl1 implements Foo {
    **     // implementation
    * }

    ? @Component
    * class FooImpl2 implements Foo {
    **     // implementation
    * }

    ? @Component
    * class Bar {
    *     private final Foo foo;

    ?    @Autowired
    *     public Bar(Foo foo) {
    *         this.foo = foo;
    *     }

    **    // rest of the class
    * }
*/

/* 
 & This is example with case using @Qualified
*/
interface MyService {
    void doSomething();
}
 
@Service("myServiceImpl1")
@Qualifier("myServiceImplementation1")
class MyServiceImpl1 implements MyService {
     public void doSomething() {
         // Implementation 1
     }
}
 
@Service("myServiceImpl2")
@Qualifier("myServiceImplementation2")
class MyServiceImpl2 implements MyService {
    public void doSomething() {
        // Implementation 2
    }
}

@Service
class MyServiceConsumer {
    @Autowired
    @Qualifier("myServiceImplementation1")
    private MyService myService;

    // rest of the class code
}

/* 
 ^ Additionally Reality

 * In reality the experience dev is usually not using @Autowired so much
 * Using @Autowired to inject dependencies into a class is a convenient way to avoid manually instantiating the dependencies. 
 * However, some developers prefer to use constructor injection instead of field injection for a number of reasons.
 * One reason is that constructor injection makes it clear what dependencies a class has 
 * and what values need to be provided in order to create an instance of the class. 
 * This can make the code more readable and easier to understand. 
 * It also helps ensure that all required dependencies are provided, 
 * as the constructor will not be called if any required dependencies are missing.

 * Another reason is that constructor injection can make it easier to write unit tests, 
 * as dependencies can be mocked or stubbed more easily.

 * Using @RequiredArgsConstructor from Lombok library is a shorthand to create constructor for all final fields. 
 * It's convenient to create constructor with this annotation because you don't need to write constructor by yourself.

 * In summary, while both @Autowired and constructor injection can be used to inject dependencies into a class, 
 * using constructor injection has some benefits such as making the code more readable, easier to understand and easier to test. 
*/

/*
^ Difference some dependency in Springboot

* Example In Springboot you can replace @Component with @Repository, @Service, @Controller and @RestController.
* But in Springboot every annotation has difference way to use.

? @Component, @Service, @Repository, @Controller, @RestController
* All of these annotations (@Service, @Repository, @Controller, and @RestController) are specializations of the more general @Component annotation.

* The @Component annotation is a generic stereotype for any Spring-managed component. By marking a class with @Component, 
* Spring can detect and instantiate it during the component scanning process.

* The more specialized annotations (@Service, @Repository, @Controller, and @RestController) are used to clarify the intended role of the annotated class in the application. 
* They are technically just synonyms for @Component and don't add any additional functionality, 
* but they provide semantic meaning and help to clarify the code.

* For example, using the @Service annotation to mark a class indicates that it provides a business service, 
* while the @Repository annotation indicates that the class is a data access object. 
* Similarly, using @Controller and @RestController indicate that the class is a controller for handling web requests.

* So, the main difference between @Component and the specialized annotations is just the semantic meaning they convey.


*/
