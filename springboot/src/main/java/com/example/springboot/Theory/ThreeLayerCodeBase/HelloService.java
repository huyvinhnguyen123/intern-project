package com.example.springboot.theory.ThreeLayerCodeBase;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/*
 * This is business layer
 * Service layer will handle logic in springboot app
 * Service will interact repository

 & See example how we create service

 * To create service, you must inject @Service to let springboot know that this file is service
*/
@Service
@RequiredArgsConstructor
public class HelloService {
    // same with controller
    // we will inject Repository 
    private final HelloRepository helloRepository;

    // method service
    public String greeting() {
        // return method from repository
        return helloRepository.helloData();
    }
}
