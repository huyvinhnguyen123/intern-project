package com.example.springboot.theory.SpringComponents;

/*
 * In addition to the core concepts of Spring, if you are working with Spring MVC (Model-View-Controller), 
 * which is a web framework based on the Spring framework, 
 * there are some additional concepts and annotations that you should be familiar with. Here are some key ones:
 
 ~ Controllers: 
 * Controllers are responsible for handling incoming HTTP requests and returning responses. 
 * In Spring MVC, controllers are typically annotated with @Controller and handle requests based on URL patterns and HTTP methods. 
 * Controllers can also handle request parameters, form data, and other aspects of web requests.

 ~ Views: 
 * Views are responsible for rendering the response that is sent back to the client. 
 * Views in Spring MVC are typically implemented using templates, such as JSP (JavaServer Pages), Thymeleaf, or other view technologies. 
 * Views are used to define how the data from the model should be rendered and presented to the user.

 ~ Model: 
 * The model represents the data that is used by the view and processed by the controller. 
 * In Spring MVC, the model can be populated with data using model attributes or model objects, 
 * which are then passed to the view for rendering.

 ~ RequestMapping: 
 * This annotation is used to map a URL pattern to a controller method. 
 * It can be used to specify the URL pattern, HTTP method, and other attributes for handling incoming requests. 
 * RequestMapping is a central concept in Spring MVC for routing and handling web requests.

 ~ ModelAttribute: 
 * This annotation is used to bind request parameters or form data to model attributes in the controller method. 
 * It can be used to automatically populate the model with data from the request, 
 * making it available for further processing or rendering in the view.

 ~ RedirectAttributes: 
 * This class is used to add attributes to a redirect URL, allowing data to be passed between multiple requests. 
 * It is often used for flash messages or other temporary data that needs to be preserved across redirects.

 ~ Form handling: 
 * Spring MVC provides support for handling form submissions, including data validation, binding form data to model attributes, 
 * and handling form submission errors.

 ~ ViewResolver: 
 * This is a configuration component that maps view names to actual view implementations. 
 * It determines which view technology (e.g., JSP, Thymeleaf) should be used to render the view based on the view name.

 ~ Interceptors: 
 * Interceptors are used to intercept requests and responses before they are handled by the controller or rendered by the view. 
 * Interceptors can be used for tasks such as authentication, authorization, logging, or caching.

 ~ Exception handling: 
 * Spring MVC provides support for handling exceptions that occur during request processing, 
 * including custom exception handling logic, error pages, and exception resolvers.
*/
/* 
 * Here are some commonly used annotations in Spring MVC:
 
 ? @Controller: 
 * This annotation is used to mark a class as a controller, 
 * which is responsible for handling incoming HTTP requests and returning responses.

 ? @RequestMapping: 
 * This annotation is used to map a URL pattern or HTTP method to a controller method. 
 * It can be used to specify the URL pattern, HTTP method, and other attributes for handling incoming requests.

 ? @GetMapping, @PostMapping, @PutMapping, @DeleteMapping: 
 * These annotations are shorthand versions of @RequestMapping specifically for GET, POST, PUT, and DELETE HTTP methods, respectively. 
 * They can be used to map a URL pattern to a controller method for a specific HTTP method.

 ? @RequestParam: 
 * This annotation is used to bind request parameters to method parameters in the controller method. 
 * It can be used to extract data from query parameters or form data.

 ? @PathVariable: 
 * This annotation is used to bind path variables from the URL to method parameters in the controller method. 
 * Path variables are parts of the URL that can be used as placeholders for dynamic values.

 ? @ModelAttribute: 
 * This annotation is used to bind request parameters or form data to model attributes in the controller method. 
 * It can be used to automatically populate the model with data from the request.

 ? @ResponseBody: 
 * This annotation is used to indicate that the return value of a controller method should be serialized directly into the response body, 
 * rather than being resolved as a view.

 ? @Valid and @Validated: 
 * These annotations are used to enable data validation in the controller methods. 
 * They can be used in conjunction with validation annotations on form objects or command objects to perform data validation.

 ? @SessionAttributes: 
 * This annotation is used to specify model attributes that should be stored in the session between multiple requests. 
 * It can be used to preserve data across multiple requests, such as in multi-step forms.

 ? @ModelAttribute: 
 * This annotation is used to mark a method in a controller as a handler method for model attribute initialization. 
 * It can be used to populate model attributes with data before the controller method is invoked.

 ? @ExceptionHandler: 
 * This annotation is used to define custom exception handling logic in the controller. 
 * It can be used to handle exceptions that occur during request processing, such as validation errors or business logic errors. 
*/