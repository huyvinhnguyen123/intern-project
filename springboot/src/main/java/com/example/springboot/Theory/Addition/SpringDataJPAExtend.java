package com.example.springboot.theory.Addition;

/* 
 ^ What is Spring Data JPA?

 * Spring Data JPA is built on top of Hibernate, 
 * which is a popular open-source Object-Relational Mapping (ORM) framework for Java. 
 * Spring Data JPA provides an abstraction layer on top of Hibernate, 
 * making it easier to work with databases in a Spring-based application. 
 * It simplifies the implementation of the data access layer by providing a set of convenient 
 * and consistent APIs for working with data.
*/

/* 
 * In Spring Data JPA, you'll need to know the following:
 
 * Entities: Entities are Java classes that are mapped to database tables using JPA annotations. 
 * They represent the data that will be stored in the database.

 * Repositories: Repositories are interfaces that define the methods for accessing and modifying the data in the database. 
 * Spring Data JPA provides a lot of the common database operations, 
 * like finding a single record by ID or finding all records, out of the box. 
 * You can also define your own custom methods for more complex queries.

 * EntityManager: The EntityManager is the main object responsible for managing entities and their lifecycle. 
 * It provides methods for persisting, merging, and removing entities, as well as querying the database.

 * Transactions: Transactions are used to ensure that a series of database operations either all succeed or all fail together. 
 * In Spring Data JPA, transactions are managed automatically, but you can also define them manually if needed.

 * QueryDSL: QueryDSL is a library that provides a type-safe way of building complex queries. 
 * It can be used in conjunction with Spring Data JPA to create more advanced queries.

 * Auditing: Spring Data JPA provides built-in support for auditing. 
 * This means that you can automatically track the creation and modification of entities, 
 * including who made the changes and when they were made.
*/

/* 
 ^ Difference between JpaRepository & CrudRepository 

 * JpaRepository is an interface that extends the PagingAndSortingRepository and adds additional JPA-related methods, 
 * such as flushing the persistence context and deleting records in a batch.

 * CrudRepository is the basic interface that provides CRUD (create, read, update, delete) operations on entities.

 * Both interfaces provide common methods for accessing data and managing entities in a database, 
 * but JpaRepository adds additional methods for JPA-specific operations. 
 * If you are using JPA, it is recommended to use JpaRepository instead of CrudRepository.
*/

/* 
 ^ Some useful annotation to create entity not from SpringDataJPA, Javax persistence, Lombok

 * Before using lombok make sure that you learned Java core clearly. 
 * Because using lombok this mean the library will generate code for you to use
 * Constructor, Getter, Setter, ToString,...
 * If you not ensure java core i suggest you have to write constructor, getter, setter,... by yourself.

 ? @Data
 * This annotation from the Lombok library includes several other annotations, including:
    ? @Getter 
    * generate getters for all non-static fields
    ? @Setter 
    * generate setters for all non-final non-static fields
    ? @ToString 
    * generate toString() method implementation
    ? @EqualsAndHashCode
    * generate equals() and hashCode() methods implementation
    ? @NoArgsConstructor
    * generate no-args constructor
    ? @AllArgsConstructor
    * generate constructor with all fields as arguments
    ? @RequiredArgsConstructor
    * generate constructor with all final fields as arguments

 ? @Entity 
 * This annotation is use to create entity(table) in database

 ? @Table 
 * This annotation is used to custom entity(table) name in database
 * if in java you create Book class then [Book] will automatically generate [book] in database
 * but if you use @Table will custom [book] to [my_book] like this
 
 ? @Id
 * This annotation is used to specify the primary key of an entity class,
 * which uniquely identifies each entity instance. 
 * It can be applied to a field or property of the entity class, 
 * and supports different types of primary key generation strategies.
 
 ? @Column
 * This annotation is used to specify the mapping of a single entity attribute to a database column. 
 * It can be applied to a field or property of the entity class, 
 * and supports various attributes such as name, nullable, length, precision, and scale.
 
 ? @Transient
 * This anntation is used to indicate that a field should not be persisted in the database when using technologies such as JPA or Hibernate. 
 * When an entity object is saved to the database, all of its fields are automatically persisted, 
 * unless marked with @Transient.
 
 ? @EntityListeners 
 * This annotation is used to specify callback listeners for an entity. 
 * It is typically used in conjunction with JPA's entity listener feature to receive lifecycle events 
 * such as before and after an entity is persisted, updated, or deleted from the database.
 * In the context of auditing, @EntityListeners(AuditingEntityListener.class) is used to specify 
 * that the entity should be audited using the auditing features provided by Spring Data JPA. 
 * This allows you to automatically track changes to your entity's data and metadata 
 * such as creation time, last modification time, and who created or modified the entity.
 
 ? @ CreatedDate, @CreatedBy, @LastModifiedDate, and @LastModifiedBy
 * All This annotation is just like created_at and updated_at field in database
 * Now we don't need to declare these fields any more we use these annotation.
 * @CreateDate, @LastModifiedDate is created_at and updated_at in entity.
 * @CreateBy, @LastModifiedBy is username who is created and updated entity.
 ! Note: to use This annotation you must declare This annotationo @EntityListeners to use them.

 ? @OneToOne
 * This annotation is used to relate one to one relationship in database
 ? @OneToMany
 * This annotation is used to relate one to many relationship in database
 ? @ManyToOne
 * This annotation is used to relate many to one relationship in database
 ? @ManyToMany
 * This annotation is used to relate many to many relationship in database
 ? @JoinColumn
 ? ...
 * All This annotations is use to relate entity(table) in database.

*/