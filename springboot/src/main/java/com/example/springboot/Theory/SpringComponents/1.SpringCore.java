package com.example.springboot.theory.SpringComponents;

/* 
 * Spring Core is a fundamental module of the Spring framework that provides the foundation for building Java applications with Spring. 
 * It includes features such as inversion of control (IoC) and dependency injection (DI), which are at the core of the Spring framework. 
 * Here are some key concepts you should be familiar with when working with Spring Core:
 
 ~ Inversion of Control (IoC): 
 * This is a design pattern where the control of creating and managing objects is inverted from the application to a framework or container. 
 * In the context of Spring, IoC means that the Spring container is responsible for creating and managing objects (beans) in the application, rather than the application itself.

 ~ Dependency Injection (DI): 
 * This is a pattern where objects receive their dependencies from the outside, rather than creating them internally. 
 * In Spring, DI is achieved through the Spring container injecting dependencies into beans, 
 * typically through constructor injection, setter injection, or method injection.

 ~ Beans: 
 * Beans are objects managed by the Spring container. 
 * They are defined in the Spring configuration file (usually XML, but can also be Java-based or annotation-based) 
 * and can be wired together with their dependencies using DI. 
 * Beans are the building blocks of a Spring application and can be used to represent various components, such as services, repositories, controllers, etc.

 ~ Spring Configuration: 
 * This is the configuration file that defines how beans are created and wired together in a Spring application. 
 * It can be written in XML, Java-based, or annotation-based format, and it specifies the beans, their dependencies, and how they are configured.

 ~ Bean Scopes: 
 * Spring supports different scopes for beans, such as singleton, prototype, request, session, etc. 
 * These define the lifecycle and behavior of beans, such as whether they are created once 
 * and shared (singleton) or created multiple times (prototype) during the lifetime of an application.

 ~ Spring Context: 
 * The Spring context is the central container that manages the beans and their dependencies. 
 * It is responsible for creating, configuring, and wiring beans together. 
 * The context can be loaded and managed in different ways, such as through XML configuration, 
 * Java-based configuration, or annotation-based configuration.

 ~ AOP (Aspect-Oriented Programming): 
 * Spring Core also includes support for AOP, 
 * which is a programming paradigm that allows for modularization of cross-cutting concerns, 
 * such as logging, caching, and security, across different components of an application.
*/
/*
 * Here are some commonly used annotations in Spring Core:
 
 ? @Component: 
 * This annotation is used to mark a class as a Spring bean/component. 
 * Beans annotated with @Component are automatically detected and registered in the Spring context, 
 * making them available for dependency injection and other Spring features.

 ? @Autowired: 
 * This annotation is used for automatic dependency injection. 
 * It can be used to inject dependencies (i.e., other beans) into a bean, 
 * either by constructor injection, setter injection, or field injection.

 ? @Qualifier: 
 * This annotation is used to specify which bean should be injected when there are multiple beans of the same type. 
 * It is used in conjunction with @Autowired to disambiguate between beans with the same type.

 ? @Value: 
 * This annotation is used to inject external values (e.g., configuration properties) into beans. 
 * It can be used to inject values from property files, environment variables, system properties, or other sources.

 ? @Scope: 
 * This annotation is used to define the scope of a bean, such as singleton, prototype, request, session, etc. 
 * It is used in conjunction with @Component to specify the lifecycle and behavior of the bean.

 ? @PostConstruct and @PreDestroy: 
 * These annotations are used to specify callback methods that should be executed after a bean is constructed and before a bean is destroyed, respectively.

 ? @Configuration: 
 * This annotation is used to mark a class as a configuration class that defines beans and their dependencies. 
 * It is typically used with Java-based configuration, where beans and their dependencies are defined as methods in a configuration class.

 ? @Bean: 
 * This annotation is used to define a bean method in a @Configuration class. 
 * It indicates that the return value of the method should be registered as a bean in the Spring context.

 ? @Profile: 
 * This annotation is used to specify which profile(s) a bean should be active in.
 *  Profiles are used to define different sets of beans for different environments or application configurations.
*/
