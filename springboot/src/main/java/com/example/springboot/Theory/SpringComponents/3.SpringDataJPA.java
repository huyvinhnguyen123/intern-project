package com.example.springboot.theory.SpringComponents;

/*
 * Here are some key concepts and technologies that you should be familiar with when working with Spring Data:
 
 ~ Data sources: 
 * Familiarize yourself with different types of data sources 
 * such as relational databases (e.g., MySQL, PostgreSQL, Oracle), NoSQL databases (e.g., MongoDB, Cassandra), 
 * and other data storage technologies.

 ~ ORM (Object-Relational Mapping): 
 * Understand the basics of ORM, which is a technique used to map objects in object-oriented programming languages to relational databases. 
 * Spring Data provides support for various ORM frameworks such as JPA (Java Persistence API), Hibernate, and others.

 ~ Repository interfaces: 
 * Learn how to define and use repository interfaces, which are interfaces that extend from Spring Data's Repository or its sub-interfaces. 
 * Repository interfaces define common data access operations such as querying, saving, updating, 
 * and deleting data, which can be automatically implemented by Spring Data based on naming conventions or custom method signatures.

 ~ Query methods: 
 * Understand how to define query methods in repository interfaces, 
 * which allow you to define custom data retrieval or manipulation operations using method names. 
 * Spring Data automatically generates the appropriate SQL or JPQL queries based on the method names, 
 * reducing the need for writing complex SQL or JPQL queries manually.

 ~ Pagination and sorting: 
 * Learn how to implement pagination and sorting for large data sets using Spring Data's built-in support. 
 * This can help you optimize performance and improve user experience when dealing with large data sets.

 ~ Auditing and caching: 
 * Understand how to enable auditing of data changes and implement caching using Spring Data's annotations 
 * such as @Audited, @Cacheable, and @CacheEvict. 
 * These features can help you track data modifications and improve performance by caching frequently accessed data.

 ~ Transaction management: 
 * Understand how to manage transactions in Spring Data using the @Transactional annotation, 
 * which allows you to specify transactional behavior for data access operations. 
 * Familiarize yourself with different transaction propagation behaviors, isolation levels, and transaction management configurations.

 ~ Custom queries: 
 * Learn how to define custom SQL or JPQL queries using the @Query annotation, 
 * which allows you to write complex queries that are not covered by the built-in CRUD operations provided by Spring Data. 
 * Understanding how to write custom queries can be helpful when dealing with complex data retrieval or manipulation scenarios.

 ~ Auditing and error handling: 
 * Understand how to handle errors and exceptions when working with Spring Data, 
 * such as handling database connectivity issues, constraint violations, and other data-related errors. 
 * Familiarize yourself with best practices for error handling and logging in Spring Data applications.

 ~ Testing: 
 * Learn how to write effective unit tests and integration tests for your Spring Data repositories and data access operations. 
 * Familiarize yourself with tools and libraries such as JUnit, Mockito, and Spring Test that can help you write comprehensive and reliable tests.  
*/
/*
 * In Spring Data, which is a part of the larger Spring ecosystem, there are several annotations that are commonly used to facilitate data access and manipulation. 
 * Here are some key annotations to be aware of:

 ? @Repository: 
 * This annotation is used to mark a class as a repository, which is responsible for data access operations. 
 * It typically encapsulates database operations such as querying, saving, updating, and deleting data.

 ? @Entity: 
 * This annotation is used to mark a class as an entity, which represents a persistent data object that can be mapped to a database table. 
 * It typically corresponds to a database table and its attributes represent the columns of the table.

 ? @Id: 
 * This annotation is used to mark a field or property in an entity class as the primary key of the corresponding database table. 
 * It is used to uniquely identify each record in the table.

 ? @Column: 
 * This annotation is used to specify the mapping between an entity attribute and a database column. 
 * It can be used to specify the column name, data type, length, and other attributes of the column.

 ? @GeneratedValue: 
 * This annotation is used to specify how a primary key should be generated for a new entity. 
 * It can be used to specify strategies such as auto-increment, identity, or sequence-based generation.

 ? @OneToMany and @ManyToOne: 
 * These annotations are used to specify relationships between entities in a one-to-many or many-to-one relationship. 
 * They can be used to specify the mapping between entities and their associated entities in a relational database.

 ? @Query: 
 * This annotation is used to define custom SQL or JPQL (Java Persistence Query Language) queries for data retrieval or manipulation. 
 * It can be used to specify complex queries that are not covered by the built-in CRUD (Create, Read, Update, Delete) operations provided by Spring Data.

 ? @Transactional: 
 * This annotation is used to mark a method or class as transactional, which means that it is executed within a transaction context. 
 * It can be used to specify transactional behavior for data access operations, ensuring data consistency and integrity.

 ? @Cacheable and @CacheEvict: 
 * These annotations are used to enable caching of data retrieved from the data source, allowing for faster subsequent access to the same data. 
 * They can be used to improve performance by caching frequently accessed data in memory.

 ? @Audited: 
 * This annotation is used to enable auditing of data changes, such as tracking who made the changes and when. 
 * It can be used to keep track of data modifications for auditing or compliance purposes. 
*/