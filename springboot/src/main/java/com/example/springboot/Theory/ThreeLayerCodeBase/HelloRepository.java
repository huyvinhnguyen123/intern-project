package com.example.springboot.theory.ThreeLayerCodeBase;

import org.springframework.stereotype.Repository;
/*
 * This is data access layer call repository
 * Repository will interact with data in database
 * Like CRUD(Create, Read, Update, Delete)

 & See example how we create service

 * To create repository, you must inject @Repository to let springboot know that this file is repository
*/
@Repository
public class HelloRepository {
    // method from repository
    public String helloData() {
        return "hello world!";
    }
}
