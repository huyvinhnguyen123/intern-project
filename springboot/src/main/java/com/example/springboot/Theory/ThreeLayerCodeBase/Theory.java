package com.example.springboot.theory.ThreeLayerCodeBase;

/* 
 * Like i said you must know what is DI & IOC? 
 * If you don't know what is this you must read again before reading this
*/

/* 
 * This is 3 layer code base in springboot
 * Before to know 3 layer code base in springboot
 * I suggest you need to know Spring MVC
 * MVC is mean Controller - View - Model
  
 * We have 3 layer code base
 * 1. Presentation Layer
 * 2. Business Layer
 * 3. Data Access Layer
 
 * We will go to detail in one by one layer
 
 ? Presentation Layer
 * This layer will contain Controller - View - Model or API(if it have)
 * It's very useful for interacting with user
 
 * If you don't know how MVC work i will explain here. 
 * Data from Model to Controller after that it will send to View to appear. 
 * On other hand if have new other request from View then it will send to Controller after that Controller will change data in model
 
 ? Business Layer
 * This layer will contain logic business here. It will also contain logic code in here
  
 ? Data Access Layer
 * This Layer will interact with database and return result to Business Layer
 
 * Because Springboot is wrapper for spring, we are still using other spring modules below, for example Spring MVC. 
 * And of course if we are using SpringMVC we will follow MVC rule.
 
 * MVC will divind 3 path
 ~Model:
 * This path is use to handle logic, interact with data.
 ~View:
 * is interface, use to show data for user to interact.
 ~Controller:
 * Connect View and Model, and control the data. 
 
 * You can see that MVC only describes a stream of data, it didn't say exactly where the code is putting like (Controller, View or Model)
 * and how to save to dabase or model. So that reason why Springboot application will work on by combine three layer with MVC together.
 
 * Combine combine three layer with MVC together we have
 ~ Controller:
 * Return view (where they already have it like HTML), or model where it's appear like API for view.
 * View can write by React, Vue or Angular)
 ~ Service:
 * Service is where have logic code interact with data. When controller give request, Service will handle request
 * and respone for controller(return model)
 ~ Repository:
 * Service can interact with another service, or use Repository to call DB. 
 * Repository it's the one who is direct interact data in database CRUD method and return to service
 
 * So like i said before then where Model and View go?
 
 * Model normal is just like Service logic is done and return to controller
 
 * View have 2 type:
 * One is direct return HTML have data. 
 * In this time controller will pass data to view and return(SpringMVC have jsp or template engine like Thymeleaf)
 * One is not from springboot will have in system with using API. View will have write another file like React, Vue, Angular
 * Controller will give data Model with using API to view and giving request from API too.  
*/
