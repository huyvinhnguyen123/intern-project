package com.example.springboot.theory.SpringSecurityExample.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/example")
public class ExampleController {
    
    @GetMapping("/hello")
    public ResponseEntity<String> SayHello() {
        return ResponseEntity.ok("Hello Spring Security");
    }

    @GetMapping("/goodbye")
    public ResponseEntity<String> SayGoodBye() {
        return ResponseEntity.ok("GoodBye Spring Security");
    }
}
