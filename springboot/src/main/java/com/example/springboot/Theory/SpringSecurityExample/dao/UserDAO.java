package com.example.springboot.theory.SpringSecurityExample.dao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {
    private final static List<UserDetails> APPLICATION_USERS = Arrays.asList(
        new User(
            "huyanphat113@gmail.com", 
            "Lumina", 
            Collections.singleton(new SimpleGrantedAuthority("ROLE_ADMIN"))
        ),

        new User(
            "shaxi.mail@gmail.com", 
            "Shaxi", 
            Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"))
        ),

        new User(
            "artifar.mail@gmail.com", 
            "Artifar", 
            Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"))
        )
    );

    public UserDetails findUserByEmail(String email) {
        return APPLICATION_USERS
                .stream()
                .filter(u -> u.getUsername().equals(email))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("No user was found"));
    }
}
