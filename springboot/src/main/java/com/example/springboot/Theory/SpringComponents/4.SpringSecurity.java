package com.example.springboot.theory.SpringComponents;

/*
 * When working with Spring Security, a powerful and widely used security framework for Java applications, 
 * you should be familiar with the following key concepts and technologies:

 ~ Authentication and Authorization: 
 * Understand the concepts of authentication (verifying the identity of a user) and authorization (granting access to resources based on user's roles or permissions). 
 * Learn how to configure authentication mechanisms such as form-based authentication, basic authentication, OAuth, and other authentication providers.

 ~ Security Configurations: 
 * Familiarize yourself with the configuration of security settings in Spring Security. 
 * This includes defining security rules, configuring access control, specifying authentication providers, handling authentication failures, and setting up session management.

 ~ User Management: 
 * Understand how to manage user accounts, roles, and permissions in Spring Security. 
 * This includes integrating with various authentication providers, such as LDAP, OAuth, and custom authentication providers. 
 * Learn how to implement features such as password encryption, account locking, password policies, and password recovery.

 ~ Security Annotations: 
 * Learn how to use Spring Security's annotations to secure your application's endpoints or methods, such as @Secured, @PreAuthorize, @PostAuthorize, and @RolesAllowed. 
 * These annotations allow you to define fine-grained access control at the method or endpoint level.

 ~ Cross-Site Scripting (XSS) and Cross-Site Request Forgery (CSRF) Protection: 
 * Understand how to protect your application against common security vulnerabilities such as XSS and CSRF attacks using Spring Security's built-in features. 
 * This includes implementing measures such as input validation, output encoding, and CSRF tokens.

 ~ Security Filters: 
 * Familiarize yourself with the different security filters provided by Spring Security, 
 * such as UsernamePasswordAuthenticationFilter, BasicAuthenticationFilter, FilterSecurityInterceptor, and others. 
 * Learn how these filters work together to provide a comprehensive security framework for your application.

 ~ Session Management: 
 * Understand how to configure and manage sessions in Spring Security, including handling session timeouts, invalidating sessions, and managing session concurrency. 
 * Learn how to implement features such as single sign-on (SSO) and session fixation protection.

 ~ Remember-Me Authentication: 
 * Learn how to implement "Remember Me" functionality in your application, which allows users to authenticate once and remain logged in across multiple sessions or visits.

 ~ Customization and Extension: 
 * Understand how to customize and extend Spring Security to meet the specific security requirements of your application. 
 * This includes creating custom authentication providers, implementing custom filters, and extending or overriding default behavior.

 ~ Testing: 
 * Learn how to effectively test your Spring Security configurations and features using tools such as JUnit, Mockito, and Spring Test. 
 * Familiarize yourself with best practices for testing security configurations, handling authentication, and simulating security scenarios.
*/
/* 
 * Here are some commonly used annotations in Spring Security:
 
 ? @EnableWebSecurity: 
 * This annotation is used to enable Spring Security for a web application. 
 * It should be added to the main configuration class of your Spring application and allows you to configure security settings.

 ? @Secured: 
 * This annotation is used to apply method-level security to a specific method. 
 * You can specify roles or permissions that are required to access the annotated method.

 ? @PreAuthorize and @PostAuthorize: 
 * These annotations are used to apply method-level security using SpEL (Spring Expression Language) expressions. 
 * @PreAuthorize is used to specify a pre-authorization check that is evaluated before the method is invoked, 
 * while @PostAuthorize is used to specify a post-authorization check that is evaluated after the method has been invoked.

 ? @RolesAllowed: 
 * This annotation is used to specify the roles that are allowed to access a particular method. 
 * It is commonly used with Java EE security and is supported in Spring Security as well.

 ? @AuthenticationPrincipal: 
 * This annotation is used to access the currently authenticated user principal in a method argument. 
 * It is commonly used to retrieve the details of the currently logged-in user.

 ? @Secured, @PreAuthorize, @PostAuthorize, and @RolesAllowed 
 * are part of the method-level security annotations in Spring Security, 
 * allowing you to secure individual methods in your application based on roles, permissions, or other custom criteria.

 ? @EnableGlobalMethodSecurity: 
 * This annotation is used to enable global method-level security in a Spring application. 
 * It allows you to configure the use of method-level security annotations 
 * such as @Secured, @PreAuthorize, and @PostAuthorize on a global level.

 ? @EnableOAuth2Sso: 
 * This annotation is used to enable OAuth2 Single Sign-On (SSO) in a Spring Boot application. 
 * It simplifies the configuration for OAuth2-based authentication with external providers such as Google, Facebook, and GitHub.

 ? @EnableResourceServer: 
 * This annotation is used to enable a resource server in a Spring Boot application. 
 * It is used in combination with OAuth2 authentication to secure RESTful APIs.

 ? @EnableAuthorizationServer: 
 * This annotation is used to enable an authorization server in a Spring Boot application. 
 * It is used in combination with OAuth2 authentication to implement OAuth2 authorization flows for granting access to protected resources.
*/