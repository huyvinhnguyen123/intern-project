package com.example.springboot.theory.ThreeLayerCodeBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

/*
 * As you know springboot has 3 layer code base
 * First we are in controller that's mean we are in Presentation Layer
 
 * So in this Presentation Layer simply will handle controller
 * The controller will interact with service
 
 & See example how we create controller

 ! If you are using Rest you should use rest controller instead controller
*/

@RestController
@RequiredArgsConstructor
public class HelloController{
    // inject service to controller
    @Autowired
    private HelloService service1;

    // normally people usually inject @Autowired
    // in project they prefer using lombok if you are using lombok you should use
    // constructor, getter, setter injection
    // don't forget to inject @RequiredArgsConstructor
    private final HelloService service2;

    public String hello1() {
        // now we call method from service 
        return service1.greeting();
    }

    public String hello2() {
        // now we call method from service 
        return service2.greeting();
    }
}