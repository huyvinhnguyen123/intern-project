package com.example.springboot.theory.Addition;

/*   
 * It's generally a good practice to write unit tests for your Spring Boot service classes before writing the actual code. 
 * Writing unit tests first helps you to define the requirements and expectations of the service class in a clear and concise way. It also helps you to identify potential edge cases and error scenarios that the service class needs to handle.

 * In addition, writing unit tests can save you a lot of time and effort in the long run. 
 * By catching bugs early on, you can avoid spending time debugging your code later, 
 * and you can also catch errors that might not be immediately apparent from the code itself.

 * By writing unit tests first, you can also help ensure that your code is more modular and maintainable. 
 * If your service class is properly designed and tested, it will be easier to make changes or refactor the code later on, 
 * without worrying about breaking other parts of the system.

 * Overall, writing unit tests for your Spring Boot service classes is a best practice 
 * that can help you write more reliable and maintainable code.
*/

/*
 * In general, there are two common types of tests that you can use to test your Spring Boot service classes:

 * Unit tests - These tests focus on testing individual units of code in isolation, 
 * typically by mocking out dependencies that the unit depends on. In Spring Boot, 
 * you can use a mocking framework like Mockito to create mock objects of your dependencies 
 * and verify the behavior of your service class.

 * Integration tests - These tests focus on testing how different parts of your application work together in a more real-world scenario. 
 * In Spring Boot, you can use the built-in testing framework and tools, 
 * like Spring Test and Spring Boot Test, to create integration tests that test the behavior of your service class along with its dependencies 
 * and other components of the application.

 * Both unit tests and integration tests have their own advantages and disadvantages, 
 * and which one you choose to use depends on your specific testing needs and goals. 
 * In general, unit tests are faster and more focused on individual units of code, 
 * while integration tests are slower but can help you catch issues that might arise 
 * when different parts of your application are put together.
 */
/* 
 ! Remember unit test and integration test is different
*/
/*
 * Basically 1 method unit test is created will have 4 path:
 * 1. Create mock data
 * 2. Define behavior of repository
 * 3. Call service method
 * 4. Assert the result and ensure that the repository is called (In case you use void method you don't need to use that)
*/
/*
 * Remember Junit 5 is difference with Junit 4 there for one your code work at junit 4 and you want to paste to junit 5 it's not working
 * Because this theory file it's not in test folder so i can't demo clearly for you just copy this code and paste to test folder
 * You will see difference between them  
*/
/* 
 ^ Here is difference between Junit 5 with Junit 4
 
 * JUnit 5 is a major update to JUnit 4 and introduces many changes and improvements. 
 * Some of the key differences between JUnit 4 and JUnit 5 are:

 * JUnit 5 has a new architecture: JUnit 5 is built on top of three main components: 
 * the Jupiter API, the Vintage Engine, and the Platform. 
 * The Jupiter API is the new programming model for writing tests, 
 * the Vintage Engine provides support for running JUnit 3 and 4 tests on the JUnit 5 platform, 
 * and the Platform provides a foundation for launching tests on various JVMs and environments.

 * JUnit 5 supports Java 8 and above: JUnit 5 requires Java 8 or later,
 * whereas JUnit 4 only requires Java 5 or later.

 * JUnit 5 introduces new features: JUnit 5 introduces several new features, 
 * such as parameterized tests, nested tests, test interfaces, and dynamic tests. 
 * These features make it easier to write flexible and reusable tests.

 * JUnit 5 has different annotations: JUnit 5 has a new set of annotations for test classes and methods, 
 * such as @Test, @BeforeEach, @AfterEach, @BeforeAll, and @AfterAll. 
 * These annotations are used to specify the test methods, lifecycle methods, and other test-related behaviors.

 * JUnit 5 has improved extension support: JUnit 5 has a new extension model that allows developers to extend the behavior of the test engine and the test framework. 
 * This model provides more flexibility and customization options compared to JUnit 4.

 * Overall, JUnit 5 is a more modern and powerful testing framework compared to JUnit 4, 
 * and provides many new features and improvements for writing effective and maintainable tests.
*/
/* 
 * In some my experience i would like to show my tips when i write unit test
 
 * When you write unit test such as getDataById(), updateData(), deleteData()
 * You don't need to write save() method because Springboot is automatically understand what you are writing
 * and because the save() method you have written in service class already so by the way in unit test you dont'
 * need to write them
*/