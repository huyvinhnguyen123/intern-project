package com.example.springboot.questions;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
* This file will contains all database question in reality project
*/
/* 
^ 1. The huge data you have worked for, how many records you have handled?

* If you said 10000 or 50000 or 100000 it's very little.
* For reality project they have handled 2 or 3 million records. Seem to impossible but it's true.

* It's not uncommon for data projects to handle millions of records, especially in industries such as finance, healthcare, 
* and social media where large amounts of data are generated.

* Managing large datasets can be challenging, and it requires careful planning and optimization to ensure that the data can be processed and analyzed efficiently. 
* This may involve using specialized data storage and processing technologies such as Hadoop, Spark, or NoSQL databases.

^ 1.1. Do you know how to handle when working with many records such as 2 or 3 million records?
* When working with large datasets like 2 or 3 million records, there are several strategies and techniques that can be used to manage and process the data effectively. 
* Here are a few examples:

~ 1. Use a database management system: 
* Databases are designed to handle large volumes of data efficiently. 
* Using a database management system (DBMS) such as MySQL or PostgreSQL can help to manage and query large datasets more efficiently.

~ 2. Use indexing: 
* Indexing is a technique used to improve database query performance. 
* By creating an index on frequently queried columns, it's possible to speed up queries and reduce the time required to retrieve data.

~ 3. Use caching: 
* Caching is a technique used to store frequently accessed data in memory, 
* reducing the need to retrieve it from the database each time it's requested. 
* Caching can significantly improve application performance when working with large datasets.

~ 4. Use pagination: 
* Instead of retrieving all data at once, it's often more efficient to retrieve data in smaller batches using pagination. 
* This can help to reduce the amount of memory required and improve query performance.

~ 5. Use parallel processing: 
* When processing large datasets, it's often possible to split the data into smaller chunks and process them in parallel. 
* This can help to speed up data processing and reduce the time required to complete a task.

* Overall we seem to know 1 & 4
* But in reality it's not only 1 & 4 but also 2,3,5

* Here some example for 2,3,5

& 2. Use indexing:
* When working with large datasets, indexing can be used to speed up queries by creating a data structure that allows for faster lookup of specific values. 
* For example, in a database table with millions of rows, indexing can be used to speed up queries that search for a specific value in a particular column.
* Write in database
*==========================================================================================================*
? CREATE INDEX index_name ON table_name (column_name);
*==========================================================================================================*

& 3. Use caching
* In Spring Boot, caching can be implemented using the Spring Cache abstraction. 
* The Spring Cache abstraction provides a simple way to add caching to Spring-based applications, using a declarative caching model.
? 1. Add the spring-boot-starter-cache dependency to your project's build file.

? 2. Configure the cache manager in your application configuration file, such as application.properties:
*==========================================================================================================*
? spring.cache.type=redis # Specify the cache type, such as Redis or Ehcache
? spring.redis.host=localhost # Specify the Redis server host
? spring.redis.port=6379 # Specify the Redis server port
*==========================================================================================================*

? 3. Enable caching for specific methods in your application code using the @Cacheable annotation:
*==========================================================================================================*
? @Service
* public class MyService {

    ? @Cacheable("myCache")
    * public MyDataObject getMyData(int id) {
        ** // Method implementation goes here
    * }

* }
*==========================================================================================================*
* In this example, the @Cacheable annotation is used to specify that the getMyData method should be cached using the "myCache" cache. 
* The first time the method is called with a particular id value, the result will be cached. 
* Subsequent calls with the same id value will return the cached result instead of executing the method again.
* Spring Boot provides support for various caching providers, including Ehcache, Redis, and others. 
* You can choose the caching provider that best suits your needs and configure it in your application configuration file.

& 5. Use parallel processing:
* In Spring Boot, parallel processing can be implemented using Java's built-in Executor framework. 
* The Executor framework provides a simple way to execute tasks in parallel across multiple threads,
* making it possible to take advantage of multi-core CPUs and improve application performance.
? 1. Define a ThreadPoolExecutor bean in your Spring configuration file, such as applicationContext.xml:
*==========================================================================================================*
? <bean id="taskExecutor" class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
?   <property name="corePoolSize" value="10" />
?   <property name="maxPoolSize" value="100" />
?   <property name="queueCapacity" value="10" />
? </bean>
*==========================================================================================================*
* In this example, a ThreadPoolTaskExecutor bean is defined with a core pool size of 10 threads, 
* a maximum pool size of 100 threads, and a queue capacity of 10 tasks.

? 2. Use the @Async annotation to mark a method as being eligible for asynchronous execution:
*==========================================================================================================*
? @Service
* public class MyService {

    ? @Async("taskExecutor")
    * public void doSomeTask() {
        ** // Method implementation goes here
    * }

* }
*==========================================================================================================*
* In this example, the doSomeTask method is marked with the @Async annotation and configured to use the taskExecutor bean defined in step 1. 
* This means that when the method is called, it will be executed asynchronously on a separate thread.

? 3. Invoke the asynchronous method from another method in your application code:
*==========================================================================================================*
? @Service
* public class AnotherService {

    ? @Autowired
    * private MyService myService;

    * public void doSomeThing( ){
        * myService.doSomeTask();
        ** // Method continues executing immediately, without waiting for doSomeTask to complete
    * }

* }
*==========================================================================================================*
* In this example, the doSomething method invokes the doSomeTask method asynchronously using the myService instance. 
* When the doSomeTask method is called, it will be executed on a separate thread, 
* allowing the doSomething method to continue executing immediately without waiting for the asynchronous task to complete.
! Note that the @Async annotation requires that you have the spring-boot-starter-aop dependency in your project's build file.
*/

/*
 ^ 2. In Springboot how to get list with advance search and pagination?

 * To implement a list with advanced search and pagination in Spring Boot, you can use Spring Data JPA and Spring MVC.
 * Here are the general steps to follow:
*/
/*
 ? 1. Define your entity class that maps to the database table you want to retrieve data from. For example:
*/
@Entity
class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private Double price;

    // getters and setters
}
/*
 ? 2. Define a repository interface that extends JpaRepository. For example:
*/
/*
* interface ProductRepository extends JpaRepository<Product, Long> {
    * Page<Product> findAll(Specification<Product> searchProducts, Pageable pageable);
* }
/
/*
 ? 3. Define a service class that implements business logic for retrieving data from the repository. For example:
*/
/*
* class ProductSpecifications {
    * public static Specification<Product> searchProducts(String name, String description, Double minPrice, Double maxPrice) {
        * return (root, query, cb) -> {
            * List<Predicate> predicates = new ArrayList<>();
            * if (name != null && !name.isEmpty()) {
                * predicates.add(cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
            * }
            * if (description != null && !description.isEmpty()) {
                * predicates.add(cb.like(cb.lower(root.get("description")), "%" + description.toLowerCase() + "%"));
            * }
            * if (minPrice != null) {
                * predicates.add(cb.ge(root.get("price"), minPrice));
            * }
            * if (maxPrice != null) {
                * predicates.add(cb.le(root.get("price"), maxPrice));
            * }
            * return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        * };
    * }
* }
*/
/*
? @Service
* class ProductService {
    ? @Autowired
    * private ProductRepository productRepository;

    * public Page<Product> searchProducts(String name, String description, Double minPrice, Double maxPrice, Pageable pageable) {
        * return productRepository.findAll(ProductSpecifications.searchProducts(name, description, minPrice, maxPrice), pageable);
    * }
* }
*/
/*
 * In this example, ProductSpecifications.searchProducts is a custom specification that defines the search criteria for products.
*/
/*
 ? 4. Define a controller class that handles HTTP requests and delegates to the service layer. For example: 
*/
/*
? @RestController
? @RequestMapping("/products")
* class ProductController {

    ? @Autowired
    * private ProductService productService;

    ? @GetMapping
    * public Page<Product> searchProducts(
        ? @RequestParam(required = false) String name,
        ? @RequestParam(required = false) String description,
        ? @RequestParam(required = false) Double minPrice,
        ? @RequestParam(required = false) Double maxPrice,
        ? @PageableDefault(size = 20) Pageable pageable) {
        * return productService.searchProducts(name, description, minPrice, maxPrice, pageable);
    * }
* }
*/
/*
 * In this example, the searchProducts method handles GET requests to /products and accepts query parameters for the search criteria and pagination settings. 
*/
/*
 ? 5 Create a view (e.g. HTML, JSON, XML) to display the search results and pagination controls.
 * In summary, you can use Spring Data JPA to retrieve data from a database table, 
 * define a custom specification to filter data based on search criteria, use Spring MVC to handle HTTP requests, 
 * and return a Page object that contains a subset of the filtered data based on the pagination settings.
*/

/*
 ^ 2. What is POJO class? DTO class?

 * POJO stands for Plain Old Java Object, which is a simple Java class that has private fields with public getter and setter methods. 
 * It doesn't have any special requirements or dependencies, and can be used in any Java application or framework.
 
 * A DTO, on the other hand, stands for Data Transfer Object, 
 * which is a Java class that is used to transfer data between layers of an application or between different applications. 
 * A DTO typically contains only the data that needs to be transferred, and does not have any behavior or logic. 
 * It is often used to encapsulate data from multiple sources into a single object 
 * that can be easily passed around the application. 
 
 * a DTO is essentially a special type of POJO that is used for data transfer between different layers of an application or between different applications. 
 * Like a POJO, a DTO is a simple Java class that has private fields with public getter and setter methods. 
 * The main difference is that a DTO is typically used to represent a specific subset of data that needs to be transferred between different layers or applications, 
 * whereas a POJO is used to represent an object in the domain model of an application. 
*/
/*
 ^ This is my experience when i worked with POJO and DTO

 * First of all it's seem to be you can see two of this class is the same and it is not so differencethan other
  
 ? So for POJO class
 * It's the first class or simple class it's didn't have special requirement. When you query and using this class
 * it's automatically return all properties(columns in database). Contains all column in database.
  
 ? DTO class
 * It's the custom class. When you want to query custom, specific column then you have to create DTO class.
 * In DTO you just declare some columns you want to query and it will contains all column in this class only.
 * Like i said POJO and DTO is the same when you query it's automatically contains all column in this class.
 * So DTO is need for query, select, search,...
*/