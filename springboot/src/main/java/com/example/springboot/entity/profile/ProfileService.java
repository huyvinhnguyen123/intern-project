package com.example.springboot.entity.profile;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.springboot.entity.account.Account;
import com.example.springboot.entity.account.AccountRepository;
import com.example.springboot.util.Logger;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProfileService {
    private final ProfileRepository profileRepository;
    private final AccountRepository accountRepository;

    public Profile CreateProfile(String accountId, Profile profile) {
        Optional<Account> optionalAccount = accountRepository.findById(accountId);
        Account account = optionalAccount.get();
        if(account != null){
            profile.setAccount(account);
            profile.setAvatarUrl("https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png?f=webp&w=256");
            profileRepository.save(profile);
            log.info(Logger.CREATE_SUCCESS);
        }else{
            log.info("Can't create profile because account not found" + accountId);
        }
        return profile;
    }
}
