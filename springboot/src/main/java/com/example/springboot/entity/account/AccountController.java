package com.example.springboot.entity.account;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/accounts")
public class AccountController {
    @Autowired private AccountService accountService;

    @GetMapping("")
    public ResponseEntity<List<Account>> getAccounts() {
        log.info("Requesting get accounts...");
        List<Account> accounts = accountService.getAllAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<AccountDTO> getAccountById(@PathVariable String accountId) {
        log.info("Requesting get account...");
        AccountDTO account = accountService.getAccountById(accountId);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    /* 
     * In this method we don't need to add @PathVariable because we don't add email as the path of URL this path is just like query parameter. 
     * For that you can use @RequestParam to indicate that the parameter is a query parameter
    */
    @GetMapping("/exist_email")
    public ResponseEntity<EmailDTO> getAccountByEmail(@RequestParam String email){
        log.info("Requesting get account...");
        EmailDTO account = accountService.getAccountByEmail(email);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Account> createUserAccount(@RequestBody Account account) {
        log.info("Requesting create account...");
        Account newAccount = accountService.createUserAccount(account);
        return new ResponseEntity<>(newAccount, HttpStatus.OK);
    }

    @PostMapping("/create/admin")
    public ResponseEntity<Account> createAdminAccount(@RequestBody Account account) {
        log.info("Requesting create account...");
        Account newAccount = accountService.createAdminAccount(account);
        return new ResponseEntity<>(newAccount, HttpStatus.OK);
    }

    @PutMapping("/resetPassword/{email}")
    public ResponseEntity<Account> resetPassword(@PathVariable String email, @RequestBody Account account) {
        log.info("requesting update account...");
        boolean success = accountService.resetPassword(email, account);
        if(success){
            return new ResponseEntity<Account>(HttpStatus.OK);
        }else{
            return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR); 
        }
    }

    @DeleteMapping("/{accountId}/delete")
    public ResponseEntity<Account> deleteAccount(@PathVariable String accountId) {
        log.info("Requesting delete account...");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
