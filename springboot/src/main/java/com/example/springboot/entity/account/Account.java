package com.example.springboot.entity.account;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.example.springboot.entity.profile.Profile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
@EntityListeners(AuditingEntityListener.class)
public class Account {
    @Id
    @Column(name="account_id", length = 50, nullable = false, updatable = false)
    private String accountId = UUID.randomUUID().toString();

    @Column(name="email", length = 255, nullable = false, unique = true)
    private String email;

    @Column(name="password", length = 255, nullable = false)
    private String password;

    @Transient
    private String retypePassword;

    @Column(name="role", length = 50, nullable = false)
    private Role role;

    @CreatedDate
    private ZonedDateTime createdDate = Instant.now().atZone(ZoneId.systemDefault());

    // @CreatedBy
    // private String createdBy;

    // @LastModifiedDate
    // private LocalDateTime lastModifiedDate;

    @LastModifiedBy
    private ZonedDateTime lastModifiedBy = Instant.now().atZone(ZoneId.systemDefault());

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    private Profile profile;
}
