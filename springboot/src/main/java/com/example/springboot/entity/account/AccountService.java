package com.example.springboot.entity.account;

import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springboot.exception.EntityException;
import com.example.springboot.util.Logger;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor 
public class AccountService {
    private final AccountRepository accountRepository;
//================================================================================================================================    
    public List<Account> getAllAccounts() {
        log.info(Logger.QUERY_SELECT_ALL_SUCCESS);
        return (List<Account>) accountRepository.findAll();
    }

    public AccountDTO getAccountById(String accountId){
        log.info(Logger.QUERY_SELECT_ONE_SUCCESS);
        String account = accountRepository.findByIdAndReturnId(accountId).orElseThrow(() -> new EntityException("Not found account: " + accountId));
        return new AccountDTO(account);
    }
//================================================================================================================================
    /*
     * I have 2 option for this one it's return string and one it's return object with one column
    */
    /*   
     * Remember we not return null when we not found data
     * Instead of returning null we throw an exception
    */
    /*
     * 
     * For return object option you have to create object to contain which columns you want to return 
     * 
    */
    public EmailDTO getAccountByEmail(String email) {
        log.info("Get account success!");
        String account = accountRepository.findByEmailAndReturnEmail(email).orElseThrow(() -> new EntityException("Not found account: " + email));
        return new EmailDTO(account);
    }

    /*
     * 
     * For return string option, you don't have to create object just return string and it's ok 
     * 
    */
    // public String getAccountByEmail(String email) {
    //     log.info("Get account success!");
    //     return accountRepository.findByEmail(email).orElseThrow(() -> new EntityException("Not found account: " + email));
    // }
//================================================================================================================================
    public Account createUserAccount(Account account){
        account.setRole(Role.USER);
        accountRepository.save(account);
        log.info(Logger.CREATE_SUCCESS);
        return account;
    }

    public Account createAdminAccount(Account account){
        account.setRole(Role.ADMIN);
        accountRepository.save(account);
        log.info(Logger.CREATE_SUCCESS);
        return account;
    }
//================================================================================================================================
    public boolean resetPassword(String email, Account updatedAccount) {
        EmailDTO emailFounded = getAccountByEmail(email);
        if(!emailFounded.getEmail().isEmpty()) {
            Account existingAccount = accountRepository.findByEmail(emailFounded.getEmail()).orElse(null);
            existingAccount.setPassword(updatedAccount.getPassword());
            existingAccount.setRetypePassword(updatedAccount.getRetypePassword());
            if(existingAccount.getPassword().equals(existingAccount.getRetypePassword())){
                existingAccount.setLastModifiedBy(Instant.now().atZone(ZoneId.systemDefault()));
                accountRepository.save(existingAccount);
                log.info(Logger.UPDATE_SUCCESS);
                return true;
            }else{
                log.error(Logger.UPLOAD_FAILED + "Reason: Password not match!");
                return false;
            }
        }else{
            log.error(Logger.UPDATE_FAILED + "Reason: Account not exist!");
            return false;
        }
    }
//================================================================================================================================
    public void deleteAccount(String accountId) {
        accountRepository.deleteById(accountId);
        log.info(Logger.DELETE_SUCCESS);
    }
}
