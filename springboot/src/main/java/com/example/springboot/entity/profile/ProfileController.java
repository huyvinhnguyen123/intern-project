package com.example.springboot.entity.profile;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.services.drive.model.File;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/accounts")
public class ProfileController {
    private final ProfileService profileService;
    @Autowired FileService fileService;
    @Autowired GoogleDriveFileService googleDriveService;

    @PostMapping("/{accountId}/profile/create")
    public ResponseEntity<Profile> CreateProfile(@PathVariable String accountId, @RequestBody Profile profile) {
        log.info("Requesting create profile...");
        Profile newProfile = profileService.CreateProfile(accountId, profile);
        return new ResponseEntity<>(newProfile, HttpStatus.OK);
    }

    @PostMapping("/upload")
	public ResponseEntity<Profile> uploadFile(@RequestParam("file") MultipartFile uploadfile) throws IOException {
		log.info("Requesting upload image...");
        fileService.uploadFile(uploadfile);
		return new ResponseEntity<>(HttpStatus.OK);
	}

    // @PostMapping("/google/upload")
    // public ResponseEntity<String> googleUploadFile(@RequestParam("file") MultipartFile multipartFile) {
    //     try {
    //         // Convert MultipartFile to java.io.File
    //         java.io.File file = googleDriveService.convertMultiPartToFile(multipartFile);

    //         // Upload the file to Google Drive
    //         googleDriveService.uploadFile(file);

    //         // Return a success response
    //         return ResponseEntity.ok("File uploaded successfully");
    //     } catch (IOException | GeneralSecurityException e) {
    //         // Handle exceptions
    //         e.printStackTrace();
    //         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload file");
    //     }
    // }

    @PostMapping("/google/upload")
    public ResponseEntity<String> googleUploadFile(@RequestParam("file") MultipartFile file) {
        try {
            googleDriveService.upload(file);
            return ResponseEntity.ok("File uploaded successfully.");
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error uploading file.");
        }
    }

    
    // @PostMapping("/upload")
	// public ResponseEntity<Profile> uploadFile(@RequestParam("file") MultipartFile uploadfile) {
	// 	try {
	// 		fileService.uploadFile(uploadfile);
    //         log.info(Logger.UPLOAD_SUCCESS);
	// 	} catch (Exception e) {
	// 		System.out.println(e.getMessage());
    //         log.info(Logger.UPLOAD_FAILED);
	// 		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	// 	}
	// 	return new ResponseEntity<>(HttpStatus.OK);
	// }
}
