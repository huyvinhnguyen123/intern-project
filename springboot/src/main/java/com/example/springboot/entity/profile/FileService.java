package com.example.springboot.entity.profile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;



import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FileService {
	private final ResourceLoader resourceLoader;

    public void uploadFile(MultipartFile multipartFile) throws IOException {
		// D:\WORKSPACE\Visual Studio Code\Java\Maven\intern-project\springboot\src\main\resources\store
		File file = new File(resourceLoader.getResource("classpath:store/").getFile() + "/" + multipartFile.getOriginalFilename());
		// File file = new File(ResourceUtils.getFile("classpath:store/").getAbsolutePath() + "/" + multipartFile.getOriginalFilename());

		if (file.createNewFile()) {	
			System.out.println("File is created!" + file.getAbsolutePath());
			System.out.println("Request contains, File: " + multipartFile.getOriginalFilename());
		} else {
			System.out.println("File already exists.");
			System.out.println("Request contains, File Path: " + file.getAbsolutePath());
			System.out.println("Request contains, File: " + multipartFile.getOriginalFilename());
		}

		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
		stream.write(multipartFile.getBytes());
		stream.close();
	}
}
