package com.example.springboot.entity.profile;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class GoogleDriveFileService {
    private static final String APPLICATION_NAME = "My Project 39691";
    private static final String SERVICE_ACCOUNT_KEY_FILE = "grounded-tine-382809-84c9cde19872.json";
    private static final String FOLDER_ID = "12e57klbf_SHpWKSya0ioLdDnY42fHyr3";

    public void upload(MultipartFile file) throws IOException, GeneralSecurityException {
        HttpTransport httpTransport = new NetHttpTransport();
        GsonFactory gsonFactory = GsonFactory.getDefaultInstance();

        // Load the service account credentials
        GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(SERVICE_ACCOUNT_KEY_FILE))
            .createScoped(Collections.singleton(DriveScopes.DRIVE_FILE));

        // Build the Drive API client
        Drive drive = new Drive.Builder(httpTransport, gsonFactory, (HttpRequestInitializer) credentials)
            .setApplicationName(APPLICATION_NAME)
            .build();

        // Create the file metadata
        File fileMetadata = new File();
        fileMetadata.setName(file.getOriginalFilename());
        fileMetadata.setParents(Collections.singletonList(FOLDER_ID));

        // Create the file content
        InputStreamContent mediaContent = new InputStreamContent(null, file.getInputStream());

        // Upload the file
        File uploadedFile = drive.files().create(fileMetadata, mediaContent)
            .setFields("id, webContentLink")
            .execute();

        System.out.println("File ID: " + uploadedFile.getId());
        System.out.println("File URL: " + uploadedFile.getWebContentLink());
    }

    // public void uploadFile(java.io.File file) throws IOException, GeneralSecurityException {
    //     HttpTransport httpTransport = new NetHttpTransport();
    //     GsonFactory gsonFactory = GsonFactory.getDefaultInstance();
    
    //     // Load the service account credentials
    //     GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(SERVICE_ACCOUNT_KEY_FILE))
    //             .createScoped(Collections.singleton(DriveScopes.DRIVE_FILE));
    
    //     // Build the Drive API client
    //     Drive drive = new Drive.Builder(httpTransport, gsonFactory, (HttpRequestInitializer) credentials)
    //             .setApplicationName(APPLICATION_NAME)
    //             .build();
    
    //     // Create the file metadata
    //     File fileMetadata = new File();
    //     fileMetadata.setName(file.getName());
    //     fileMetadata.setParents(Collections.singletonList(FOLDER_ID));
    
    //     // Create the file content
    //     FileContent mediaContent = new FileContent(null, file);
    
    //     // Upload the file
    //     File uploadedFile = drive.files().create(fileMetadata, mediaContent)
    //             .setFields("id, webContentLink")
    //             .execute();
    
    //     System.out.println("File ID: " + uploadedFile.getId());
    //     System.out.println("File URL: " + uploadedFile.getWebContentLink());
    // }


    // public java.io.File convertMultiPartToFile(MultipartFile file) throws IOException {
    //     java.io.File convFile = new java.io.File(file.getOriginalFilename());
    //     FileOutputStream fos = new FileOutputStream(convFile);
    //     fos.write(file.getBytes());
    //     fos.close();
    //     return convFile;
    // }
}
