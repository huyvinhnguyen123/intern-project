package com.example.springboot.entity.account;

public class AccountDTO {
    private String accountId;

    public AccountDTO(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

}
