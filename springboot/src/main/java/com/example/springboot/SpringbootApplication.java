package com.example.springboot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);


		long a = 1, b = 2, c = 3;

		List<Long> data = new ArrayList<>();
		data.add(0, a);
		data.add(0, b);
		data.add(0, c);

		Object obj = data.toArray(new Long[data.size()]);
		System.out.println("Obj");
		System.out.println("Obj: " + obj.toString());
		
	}

}
