package com.example.springboot.util;

public class Logger {
    // CRUD Logger
    public static final String CREATE_SUCCESS = "Create success!"; 
    public static final String CREATE_FAILED = "Create failed!"; 
    public static final String UPDATE_SUCCESS = "Update success!"; 
    public static final String UPDATE_FAILED = "Update failed!";
    public static final String DELETE_SUCCESS = "Delete success!"; 
    public static final String DELETE_FAILED = "Delete failed!";
    public static final String QUERY_SELECT_ALL_SUCCESS = "Query select all success!"; 
    public static final String QUERY_SELECT_ALL_FAILED = "Query select all failed!";
    public static final String QUERY_SELECT_ONE_SUCCESS = "Query select one success!";
    public static final String QUERY_SELECT_ONE_FAILED = "Query select one failed!";
    public static final String QUERY_SEARCH_SUCCESS  = "Query search success!";
    public static final String QUERY_SEARCH_FAILED = "Query search failed!";
    // Upload Logger 
    public static final String UPLOAD_SUCCESS = "Upload success"; 
    public static final String UPLOAD_FAILED = "Upload failed";
    // Payroll Logger 
    public static final String PAYROLL_SUCCESS = "Payroll success!"; 
    public static final String PAYROLL_FAILED = "Payroll failed!";
    // Not found Logger
    public static final String NOT_FOUND = "Error 404 Not found!";
}