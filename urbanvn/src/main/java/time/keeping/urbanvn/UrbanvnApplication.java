package time.keeping.urbanvn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrbanvnApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrbanvnApplication.class, args);
	}

}
