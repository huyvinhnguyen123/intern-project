package time.keeping.urbanvn.apiDemoEntity.user;

import javax.ws.rs.NotFoundException;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// @Service
public class UserServiceImple implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    @Override
    public User getUserById(String id) {
        return userRepository.getUser(findIndexById(id));
    }

    @Override
    public void saveUser(User user) {
        userRepository.saveUser(user);
    }

    @Override
    public void updateUser(String id, User user) {
        userRepository.updateUser(findIndexById(id), user);
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteUser(findIndexById(id));
    }

    private int findIndexById(String id) {
        return IntStream.range(0, userRepository.getUsers().size())
            .filter(index -> userRepository.getUsers().get(index).getUserId().equals(id))
            .findFirst()
            .orElseThrow(() -> new NotFoundException("User not found with id " + id));
    }
}
