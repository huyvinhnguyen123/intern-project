package time.keeping.urbanvn.apiDemoEntity.user;

import java.util.List;

// to achieve loose coupling when run testing
public interface UserService {
    List<User> getUsers();
    User getUserById(String id);
    void saveUser(User user);
    void updateUser(String id, User user);
    void deleteUser(String id);
}
