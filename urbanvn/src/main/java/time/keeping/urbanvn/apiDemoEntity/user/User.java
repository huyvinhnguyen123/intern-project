package time.keeping.urbanvn.apiDemoEntity.user;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
public class User {

    private String userId;
    
    @NotBlank(message = "Name cannot be blank")
    private String username;
    
    @NotBlank(message = "Email cannot be blank")
    private String email;
    
    @NotBlank(message = "Password cannot be blank")
    private String password;
    
    @NotBlank(message = "Created at cannot be blank")
    private String createdAt;
    
    @NotBlank(message = "Update at cannot be blank")
    private String updatedAt;
    
    public User(){
        this.userId = UUID.randomUUID().toString();
        this.createdAt = CurrentDate.getCurrentDateWithoutHMS();
        this.updatedAt = CurrentDate.getCurrentDateWithoutHMS();
    }
    
}
