package time.keeping.urbanvn.utils;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class ExpressionValidate {
	// validation email
	public static boolean EmailValidate(String email) {
		String regex = "^(.+)@(.+)$";
	    Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher((CharSequence) email);
	    return matcher.matches();
	}
}

