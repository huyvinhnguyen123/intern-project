package time.keeping.urbanvn.utils;

import java.util.Random;

public final class Generate {
	// generate random password
	public static String generateRandomPassword(int len) {
		String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
          +"lmnopqrstuvwxyz!@#$%&";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(chars.charAt(rnd.nextInt(chars.length())));
		return sb.toString();
	}
// =========================================================================================================================
	// generate random code
    public static char[] generateRandomCode(int length) {
		String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
	    String specialCharacters = "!@#$";
	    String numbers = "1234567890";
	    
	    // combine all characters
	    String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
	    
	    // create random
	    Random random = new Random();
	    
	    // create length for password
	    char[] password = new char[length];

	    // generate random value in index position
	    password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
	    password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
	    password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
	    password[3] = numbers.charAt(random.nextInt(numbers.length()));
	   
	    // loop index position
	    for(int i = 4; i< length ; i++) {
	    	// combine generate random value in index position 
	    	password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
	    }
	    return password;
	}
}