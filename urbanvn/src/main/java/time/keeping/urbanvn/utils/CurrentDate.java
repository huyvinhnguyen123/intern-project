package time.keeping.urbanvn.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public final class CurrentDate {
// =========================================================================================================================
	// Create current date and format local date & time 
	public static String getCurrentDate () {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd,hh:mm:ss");
		String currentDate = dateFormat.format(date);
		System.out.println(currentDate);
		return currentDate;
	}
// =========================================================================================================================
	// Create current date and format local date & time 
	public static String getCurrentDateWithoutHMS () {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = dateFormat.format(date);
		System.out.println(currentDate);
		return currentDate;
	}
// =========================================================================================================================
	// Create current date combine with random number
	public static String getCurrentDate(int randomNum) {
		Random random = new Random();
		Date startTime = new Date(random.nextLong());
		Date endTime = new Date(random.nextInt(1000) + random.nextInt(randomNum));
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String convertStartTime = dateFormat.format(startTime);
		String convertEndTime = dateFormat.format(endTime);
		String currentDate = (convertStartTime + convertEndTime);
		System.out.println(currentDate);
		return currentDate;
	}
}

