package time.keeping.urbanvn.entity.payroll;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import time.keeping.urbanvn.entity.profile.Profile;

@Data
@Entity
@Table(name = "pay_roll")
public class PayRoll {
    @ManyToOne
    @JoinColumn(name = "profile_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Profile profile;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payroll_id")
    private Integer payRollId;

    @Column(name = "pay_period_start_date")
    private String payPeriodStartDate;

    @Column(name = "pay_period_end_date")
    private String payPeriodEndDate;
    
	@Min(value = 0, message = "Total hours cannot be negative")
    @Column(name = "total_hours")
    private double totalHours;

    @Min(value = 0, message = "Hourly rate cannot be negative")
    @Column(name = "hourly_rate")
    private double hourlyRate;

    @Min(value = 0, message = "Gross pay cannot be negative")
    @Column(name = "gross_pay")
    private double grossPay;

    @Min(value = 0, message = "Taxes with held cannot be negative")
    @Column(name = "taxes_withheld")
    private double taxesWithHeld;

    @Min(value = 0, message = "Net pay cannot be negative")
    @Column(name = "net_pay")
    private double netPay;

    @Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;
}
