package time.keeping.urbanvn.entity.user;

/* 
 ^ Dependency injection

 * Dependency injection là một mẫu thiết kế và là một khái niệm cơ bản trong Spring Framework, bao gồm cả Spring Boot. 
 * Đó là một quá trình cung cấp các phụ thuộc hoặc các đối tượng cần thiết của một lớp cho nó, 
 * chứ không phải chính lớp đó tạo ra các đối tượng đó.

 * Trong Spring Boot, Dependency Injection đạt được thông qua việc sử dụng bộ chứa đảo ngược điều khiển (IoC), 
 * chịu trách nhiệm tạo các đối tượng và kết nối chúng lại với nhau. 
 * Bộ chứa IoC tạo các đối tượng dựa trên siêu dữ liệu cấu hình được cung cấp cho nó. Siêu dữ liệu cấu hình có thể ở dạng XML, 
 * Annotation Java hoặc mã Java.

 * Dependency Injection trong Spring Boot giúp đạt được sự liên kết lỏng lẻo giữa các lớp,
 * điều này làm cho mã có tính mô-đun và dễ bảo trì. 
 * Nó cũng làm cho việc kiểm tra dễ dàng hơn vì các phần phụ thuộc có thể dễ dàng bị giả lập 
 * hoặc thay thế bằng các lần kiểm tra kép. Spring Boot cung cấp một số Annotation để kích hoạt tính năng Tiêm phụ thuộc, 
 * bao gồm @Autowired, @Component, @Service, @Repository và @Controller.
 

 * Annotation @Autowired được sử dụng để tự động chèn các phụ thuộc và nó có thể được sử dụng ở cấp độ hàm tạo, 
 * trình thiết lập hoặc trường. Bộ chứa IoC giải quyết các phụ thuộc theo loại và đưa bean phù hợp vào lớp phụ thuộc.
 * Nhìn chung, Dependency Injection là một tính năng mạnh mẽ của Spring Boot 
 * giúp tạo các ứng dụng có thể bảo trì và liên kết lỏng lẻo.
 
 
 * @Component, @Service, @Repository và @Controller là các Annotation khác nhau trong Spring Framework, 
 * mặc dù chúng đều là các chuyên biệt hóa của Annotation @Component.

 * Annotation @Component là một Annotation chung được sử dụng để đánh dấu một lớp là một thành phần, 
 * có nghĩa là nó có thể được tự động phát hiện và đăng ký với bộ chứa Spring IoC để tiêm phụ thuộc.

 * @Service, @Repository và @Controller là các Annotation cụ thể hơn được sử dụng để đánh dấu các lớp trong các lớp khác nhau của ứng dụng.

 * @Service được sử dụng để đánh dấu các lớp chứa logic nghiệp vụ, chẳng hạn như các lớp thuộc tầng dịch vụ.

 * @Repository được sử dụng để đánh dấu các lớp tương tác với cơ sở dữ liệu hoặc lớp lưu trữ lâu dài, chẳng hạn như các đối tượng truy cập dữ liệu (DAO).

 * @Controller được sử dụng để đánh dấu các lớp xử lý yêu cầu web và trả về phản hồi HTTP.

 * Mặc dù các Annotation này đều là chuyên môn hóa của @Component, nhưng chúng có một số khác biệt về hành vi. Ví dụ: @Service và @Repository cung cấp chức năng bổ sung, chẳng hạn như dịch tự động các ngoại lệ cơ sở dữ liệu sang hệ thống phân cấp DataAccessException của Spring.

 * Tuy nhiên, từ quan điểm thực tế, sử dụng @Component thường là đủ cho hầu hết các trường hợp và việc chọn sử dụng @Service, @Repository hoặc @Controller có thể giúp cung cấp thêm ngữ cảnh và rõ ràng cho mã. 

*/