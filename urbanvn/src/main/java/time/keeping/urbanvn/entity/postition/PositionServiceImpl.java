package time.keeping.urbanvn.entity.postition;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class PositionServiceImpl implements PositionService {
    private final PositionRepository positionRepository;

    @Override
    public List<Position> getPositions() {
        log.info("Get positions success!");
        return (List<Position>) positionRepository.findAll();
    }

    @Override
    public Optional<Position> getPosition(Integer PositionId) {
        log.info("Get position success!");
        return positionRepository.findById(PositionId);
    }

    @Override
    public Position addPosition(Position newPosition, OfficePosition officePosition){
        newPosition.setPostionName(officePosition.getPostionName());
        positionRepository.save(newPosition);
        log.info("Add position success!");
        return newPosition;
    }

    @Override
    public List<Position> addPositions(Position position, OfficePosition officePosition) {
        List<Position> positions = new ArrayList<Position>();
        positions.add(addPosition(position, officePosition));
        return positions;
    }
}
