package time.keeping.urbanvn.entity.address.init;

public enum OfficeCity {
    NONE(""),
    YOKOHAMA("Yokohama"),
    TOKYO("Tokyo"),
    SAPPORO("Sapporo"),
    TAGAJO("Tagajo");

    private String cityName;

    OfficeCity(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }
}
