package time.keeping.urbanvn.entity.user;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
    
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/accounts")
public class AccountController {
    private final AccountService accountService;

    @GetMapping("")
    public ResponseEntity<List<Account>> getAccounts() {
        log.info("Requesting get accounts...");
        List<Account> accounts = accountService.getAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<Account> getAccountById(@PathVariable String accountId) {
        log.info("Requesting get account...");
        Optional<Account> account = accountService.getAccountById(accountId);
        return new ResponseEntity<>(account.orElse(null), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Account> createAccount(@RequestBody Account account) throws NoSuchAlgorithmException {
        log.info("Requesting create account...");
        Account newaccount = accountService.createUser(account);
        return new ResponseEntity<>(newaccount, HttpStatus.CREATED);
    }

    @PostMapping("/create/admin")
    public ResponseEntity<Account> createAdminAccount(@RequestBody Account account) throws NoSuchAlgorithmException {
        log.info("Requesting create account...");
        Account newaccount = accountService.createAdmin(account);
        return new ResponseEntity<>(newaccount, HttpStatus.CREATED);
    }

    @PutMapping("/update/{accountId}")
    public ResponseEntity<Account> updateAccount(@PathVariable String accountId, @RequestBody Account account) {
        log.info("requesting update account...");
        Account existingaccount = accountService.updateAccount(accountId, account);
        return new ResponseEntity<Account>(existingaccount, HttpStatus.OK);
    }

    @PutMapping("/resetPassword/{email}")
    public ResponseEntity<Account> resetPassword(@PathVariable String email, @RequestBody Account account) throws NoSuchAlgorithmException {
        log.info("requesting update account...");
        boolean success = accountService.resetPassword(email, account);
        if(success){
            return new ResponseEntity<Account>(HttpStatus.OK);
        }else{
            return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR); 
        }
    }

    @DeleteMapping("/delete/{accountId}")
    public ResponseEntity<Account> deleteAccount(@PathVariable String accountId) {
        log.info("requesting delete account...");
        accountService.deleteAccount(accountId);
        return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
    }
}
