package time.keeping.urbanvn.entity.timecard;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import time.keeping.urbanvn.entity.profile.Profile;

@Data
@Entity
@Table(name = "time_card")
public class TimeCard {
    @ManyToOne
    @JoinColumn(name = "profile_id", nullable = false)
    private Profile profile;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timecard_id")
    private Integer timeCardId;

    @NotBlank(message = "Start time cannot be blank")
    @Column(name = "start_time")
    private String startTime;

    @NotBlank(message = "End time cannot be blank")
    @Column(name = "end_time")
    private String endTime;

	@Min(value = 0, message = "Total hours cannot be negative")
    @Column(name = "total_hours")
    private double totalHours;

    @Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;
}
