package time.keeping.urbanvn.entity.address;

import org.springframework.data.repository.CrudRepository;

public interface WardRepository extends CrudRepository<Ward, Integer> {}
