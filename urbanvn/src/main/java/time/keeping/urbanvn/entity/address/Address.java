package time.keeping.urbanvn.entity.address;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import time.keeping.urbanvn.entity.profile.Profile;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
@Entity
@Table(name = "address")
public class Address {
    @OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "city_id", referencedColumnName = "city_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private City city;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "district_id", referencedColumnName = "district_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private District district;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "ward_id", referencedColumnName = "ward_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Ward ward;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Integer addressId;

    @NotBlank(message = "addressName cannot be blank")
    @Column(name = "address_name", length = 100, nullable = false)
    private String addressName;

    @NotBlank(message = "addressPhone cannot be blank")
    @Column(name = "address_phone", length = 15, nullable = false)
    private String addressPhone;

    @NotBlank(message = "address cannot be blank")
    @Column(name = "address", length = 255, nullable = false)
    private String address;

    @Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;

    @ManyToMany(mappedBy = "addresses")
    private List<Profile> profiles; 

    public Address(){
        this.createdAt = CurrentDate.getCurrentDate();
        this.updatedAt = CurrentDate.getCurrentDate();
    }
}
