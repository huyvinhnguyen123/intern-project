package time.keeping.urbanvn.entity.role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import time.keeping.urbanvn.entity.user.Account;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
@Entity
@Table(name = "role")
public class Role {
    @ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties("roles")
	private Account account;
	
	@Id
	@Column(name = "role_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long roleId;
	
	@Column(name = "role_name", length = 20, nullable = false)
	private String roleName;
	
	@Column(name = "role_description", length = 255)
	private String roleDescription;
	
	@Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;

    public Role() {
        this.createdAt = CurrentDate.getCurrentDate();
        this.updatedAt = CurrentDate.getCurrentDate();
    }


	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role other = (Role) obj;
        if (roleId == null) {
            if (other.roleId != null)
                return false;
        } else if (!roleId.equals(other.roleId))
            return false;
        return true;
    }
}
