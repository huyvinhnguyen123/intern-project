// package time.keeping.urbanvn.entity.profile;

// import java.security.NoSuchAlgorithmException;

// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// import lombok.RequiredArgsConstructor;
// import lombok.extern.slf4j.Slf4j;

// @RestController
// @Slf4j
// @RequiredArgsConstructor
// @RequestMapping("/profile")
// public class ProfileController {
//     private final ProfileService profileService;

//     @PostMapping("/create/{accountId}")
//     public ResponseEntity<Profile> createProfile(@PathVariable String accountId, @RequestBody Profile profile) throws NoSuchAlgorithmException {
//         log.info("Requesting create profile from account...");
//         Profile newProfile = profileService.createProfile(accountId, profile);
//         return new ResponseEntity<>(newProfile, HttpStatus.CREATED);
//     }
// }
