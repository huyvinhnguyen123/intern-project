package time.keeping.urbanvn.entity.postition;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import time.keeping.urbanvn.entity.profile.Profile;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
@Entity
@Table(name = "position")
public class Position {
    @Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "position_id")
    private Integer positonId;
    
    @NotBlank(message = "positionName cannot be blank")
    @Column(name = "position_name", length = 50, nullable = false)
    private String postionName;

    @Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;

    @ManyToMany(mappedBy = "positions")
    private List<Profile> profiles;

    public Position() {
        this.createdAt = CurrentDate.getCurrentDate();
        this.updatedAt = CurrentDate.getCurrentDate();
    }
}
