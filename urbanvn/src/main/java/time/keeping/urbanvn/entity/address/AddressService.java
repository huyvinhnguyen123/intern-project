// package time.keeping.urbanvn.entity.address;

// import java.util.List;
// import java.util.Optional;

// import time.keeping.urbanvn.entity.address.init.OfficeCity;
// import time.keeping.urbanvn.entity.address.init.OfficeDistrict;
// import time.keeping.urbanvn.entity.address.init.OfficeWard;
// import time.keeping.urbanvn.entity.profile.Profile;
// import time.keeping.urbanvn.entity.address.init.OfficeAddress;

// public interface AddressService {
//     List<City> getCities();
//     City addCity(City city, OfficeCity officeCity);
//     List<City> addCities(City city, OfficeCity officeCity);

//     List<District> getDistricts();
//     District addDistrict(District district, OfficeDistrict officeDistrict);
//     List<District> addDistricts(District district, OfficeDistrict officeDistrict);

//     List<Ward> getWards();
//     Ward addWard(Ward ward, OfficeWard officeWard);
//     List<Ward> addWards(Ward ward, OfficeWard officeWard);

//     List<Address> getListAddressesOffice();
//     Optional<Address> getAddresses(Integer addressId);
//     Address addAddress(OfficeAddress officeAddress);
//     // List<Address> addAddresses(OfficeAddress officeAddress);
// }
