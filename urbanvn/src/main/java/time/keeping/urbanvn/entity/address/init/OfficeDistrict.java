package time.keeping.urbanvn.entity.address.init;

public enum OfficeDistrict {
    NONE(""),
    NISHI("Nishi-ku"),
    MINATO("Minato-ku"),
    CHUO("Chuo-ku"),
    MIYAGI("Miyagi Prefecture");

    private String districtName;

    OfficeDistrict(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictName() {
        return districtName;
    }
}
