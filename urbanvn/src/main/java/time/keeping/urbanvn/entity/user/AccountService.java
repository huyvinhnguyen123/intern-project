package time.keeping.urbanvn.entity.user;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

public interface AccountService {
    List<Account> getAccounts();
    Optional<Account> getAccountById(String userId);
    Account createUser(Account user) throws NoSuchAlgorithmException;
    Account createAdmin(Account admin) throws NoSuchAlgorithmException;
    Account updateAccount(String userId, Account account);
    boolean resetPassword(String email, Account account) throws NoSuchAlgorithmException;
    void deleteAccount(String userId);

    Account login(Account account);
}
