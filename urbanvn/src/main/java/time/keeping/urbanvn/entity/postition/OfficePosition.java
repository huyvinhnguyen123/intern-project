package time.keeping.urbanvn.entity.postition;

public enum OfficePosition {
    STAFF("Staff"),
    LEADER("Leader"),
    MANAGER("Manager"),
    HEAD("Head of department");

    private String positionName;

    OfficePosition(String positionName) {
        this.positionName = positionName;
    }

    public String getPostionName() {
        return positionName;
    }
}
