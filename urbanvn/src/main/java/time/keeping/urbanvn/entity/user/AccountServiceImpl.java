package time.keeping.urbanvn.entity.user;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import time.keeping.urbanvn.entity.role.OfficeRole;
import time.keeping.urbanvn.entity.role.Role;
import time.keeping.urbanvn.entity.role.RoleService;
import time.keeping.urbanvn.utils.Encode;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final RoleService roleService;
// ===========================================================================================
    // create hash password
	public String HashPassword (String password) throws NoSuchAlgorithmException {
		return Encode.toHexString(Encode.GetSHA256(password));
	}

    @Override
    public List<Account> getAccounts() {
        log.info("Get users success!");
        return (List<Account>) accountRepository.findAll();
    }

    @Override
    public Optional<Account> getAccountById(String userId){
        log.info("Get user success!");
        return accountRepository.findById(userId);
    }


    
    @Override
    public Account createUser(Account newUser) throws NoSuchAlgorithmException {
        // newUser.setPassword(HashPassword(newUser.getPassword()));
        accountRepository.save(newUser);
        log.info("Add user success!");

        Set<Role> setRoles = new HashSet<Role>(roleService.addRoles(newUser, OfficeRole.USER));
		newUser.setRoles(setRoles);
        accountRepository.save(newUser);
        log.info("Update user with roles success!");

        return newUser;
    }

    @Override
    public Account createAdmin(Account newAdmin) throws NoSuchAlgorithmException {
        // newAdmin.setPassword(HashPassword(newAdmin.getPassword()));
        accountRepository.save(newAdmin);
        log.info("Add admin success!");

        Set<Role> setRoles = new HashSet<Role>(roleService.addRoles(newAdmin, OfficeRole.ADMIN));
		newAdmin.setRoles(setRoles);
        accountRepository.save(newAdmin);
        log.info("Update admin with roles success!");

        return newAdmin;
    }

    @Override
    public Account updateAccount(String accountId, Account updateAccount) {
        Account existingAccount = getAccountById(accountId).orElse(null);
        if(existingAccount != null) {
            existingAccount.setUsername(updateAccount.getUsername());
            accountRepository.save(existingAccount);
            log.info("Account updated success!");
        }else{
            log.error("Account not exist!");
        }
        return existingAccount;
    }

    @Override
    public boolean resetPassword(String email, Account updatedAccount) throws NoSuchAlgorithmException {
        Account existingAccount = accountRepository.findByEmail(email).orElse(null);
        if(existingAccount != null) {
            existingAccount.setPassword(updatedAccount.getPassword());
            existingAccount.setRetypePassword(updatedAccount.getRetypePassword());
            if(existingAccount.getPassword().equals(existingAccount.getRetypePassword())){
                accountRepository.save(existingAccount);
                log.info("Reset password success!");
                return true;
            }else{
                log.error("Password not match!");
                return false;
            }
            
        }else{
            log.error("Account not exist!");
            return false;
        }
    }

    @Override
    public void deleteAccount(String accountId) {
        accountRepository.deleteById(accountId);
        log.info("Delete account success!");
    }

    @Override
    public Account login(Account account) {
        return account;
    }
}
