// package time.keeping.urbanvn.entity.address;

// import java.util.ArrayList;
// import java.util.List;
// import java.util.Optional;

// import org.springframework.stereotype.Service;

// import lombok.RequiredArgsConstructor;
// import lombok.extern.slf4j.Slf4j;
// import time.keeping.urbanvn.entity.address.init.OfficeCity;
// import time.keeping.urbanvn.entity.address.init.OfficeDistrict;
// import time.keeping.urbanvn.entity.address.init.OfficeWard;
// import time.keeping.urbanvn.entity.profile.ProfileService;
// import time.keeping.urbanvn.entity.address.init.OfficeAddress;

// @Service
// @Slf4j
// @RequiredArgsConstructor
// public class AddressImpl implements AddressService {
//     private final AddressRepository addressRepository;
//     private final CityRepository cityRepository;
//     private final DistrictRepository districtRepository;
//     private final WardRepository wardRepository;
//     private final ProfileService profileService;
// // ===========================================================================================
//     @Override
//     public List<City> getCities() {
//         log.info("Get all cities success!");
//         return (List<City>) cityRepository.findAll();
//     }

//     @Override
//     public City addCity(City newCity, OfficeCity officeCity){
//         newCity.setCityName(officeCity.getCityName());
//         cityRepository.save(newCity);
//         log.info("Add new city success!");
//         return newCity;
//     }
    
//     @Override
//     public List<City> addCities(City city, OfficeCity officeCity) {
//         List<City> cities = new ArrayList<City>();
//         cities.add(addCity(city, officeCity));
//         return cities;
//     }
// // ===========================================================================================
//     @Override
//     public List<District> getDistricts() {
//         log.info("Get all districts success!");
//         return (List<District>) districtRepository.findAll();
//     }

//     @Override
//     public District addDistrict(District newDistrict, OfficeDistrict officeDistrict) {
//         newDistrict.setDistrictName(officeDistrict.getDistrictName());
//         districtRepository.save(newDistrict);
//         log.info("Add new district success!");
//         return newDistrict; 
//     }

//     @Override
//     public List<District> addDistricts(District district, OfficeDistrict officeDistrict) {
//         List<District> districts = new ArrayList<District>();
//         districts.add(addDistrict(district, officeDistrict));
//         return districts;
//     }
// // ===========================================================================================
//     @Override
//     public List<Ward> getWards() {
//         log.info("Get all wards success!");
//         return (List<Ward>) wardRepository.findAll();
//     }

//     @Override
//     public Ward addWard(Ward newWard, OfficeWard officeWard) {
//         newWard.setWardName(officeWard.getWardName());
//         wardRepository.save(newWard);
//         log.info("Add new ward success!");
//         return newWard;
//     }

//     @Override
//     public List<Ward> addWards(Ward ward, OfficeWard officeWard) {
//         List<Ward> wards = new ArrayList<Ward>();
//         wards.add(addWard(ward, officeWard));
//         return wards;
//     }
// // ===========================================================================================
//     @Override
//     public List<Address> getListAddressesOffice() {
//         log.info("Get all addresses success!");
//         return (List<Address>) addressRepository.findAll();
//     }

//     @Override
//     public Optional<Address> getAddresses(Integer addressId) {
//         log.info("Get address success!");
//         return addressRepository.findById(addressId);
//     }

//     @Override
//     public Address addAddress(OfficeAddress officeAddress) {
//         Address newAddress = new Address();
//         newAddress.setProfiles(profileService.getProfiles());
//         newAddress.setAddressName(officeAddress.getAddressName());
//         newAddress.setAddressPhone(officeAddress.getAddressPhone());
//         newAddress.setAddress(officeAddress.getAddress());
//         addressRepository.save(newAddress);
//         log.info("Add address success!");
//         return newAddress;
//     }

//     // @Override
//     // public List<Address> addAddresses(OfficeAddress officeAddress) {
//     //     List<Address> addresses = new ArrayList<Address>();
//     //     addresses.add(addAddress(address, officeAddress));
//     //     return addresses;
//     // }
// }
