// package time.keeping.urbanvn.entity.profile;

// import java.util.List;
// import java.util.Optional;

// import org.springframework.stereotype.Service;

// import lombok.RequiredArgsConstructor;
// import lombok.extern.slf4j.Slf4j;
// import time.keeping.urbanvn.entity.address.Address;
// import time.keeping.urbanvn.entity.address.AddressService;
// import time.keeping.urbanvn.entity.address.init.OfficeAddress;
// import time.keeping.urbanvn.entity.postition.Position;
// import time.keeping.urbanvn.entity.postition.PositionService;
// import time.keeping.urbanvn.entity.user.Account;
// import time.keeping.urbanvn.entity.user.AccountService;

// @Service
// @Slf4j
// @RequiredArgsConstructor
// public class ProfileServiceImpl implements ProfileService {
//     private final ProfileRepository profileRepository;
//     private final AccountService accountService;
//     private final AddressService addressService;
//     private final PositionService positionService;
// // ===========================================================================================
//     @Override
//     public List<Profile> getProfiles() {
//         log.info("Get profiles success!");
//         return (List<Profile>) profileRepository.findAll();
//     }

//     @Override
//     public Optional<Profile> getProfile(String profileId) {
//         log.info("Get profile success!");
//         return profileRepository.findById(profileId);
//     }

//     @Override
//     public Profile createProfile(String accountId, Profile newProfile) {
//         Account existingAccount = accountService.getAccountById(accountId).orElse(null);
//         if(existingAccount != null){
//             newProfile.setAccount(existingAccount);
//             // newProfile.setAddresses(addressService.addOfficeAddresses(newProfile, OfficeAddress.YOKOHAMA));
//             newProfile.setAvatarUrl("https://w7.pngwing.com/pngs/205/731/png-transparent-default-avatar.png");
//             newProfile.setPhoneNumber("000-000-0000");
//             profileRepository.save(newProfile);
//             log.info("Create profile success!");
//             return newProfile;
//         }else{
//             log.error("Account is not exist!");
//             return null;
//         }
//     }

//     @Override
//     public Profile updateProfile(String profileId, Profile existingProfile) {
//         existingProfile = getProfile(profileId).orElse(null);
//         if(existingProfile != null) {
//             log.info("Profile updated success!");
//         }else{
//             log.info("Profile not exist!");
//         }
//         return existingProfile;
//     }
// }
