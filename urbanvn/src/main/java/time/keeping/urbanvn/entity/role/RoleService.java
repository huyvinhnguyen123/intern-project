package time.keeping.urbanvn.entity.role;

import java.util.List;
import time.keeping.urbanvn.entity.user.Account;

public interface RoleService {
    List<Role> getRoles();
    Role addRole(Account account, OfficeRole role); 
    List<Role> addRoles (Account account, OfficeRole role);
}
