package time.keeping.urbanvn.entity.address.init;

public enum OfficeAddress {
    NONE("","",""),
    YOKOHAMA("Yokohama Office", "045-453-0035","Yokohama East Exit Wisport Building 17F, 2-6-32 Takashima, Nishi-ku, Yokohama"),
    TOKYO("Tokyo office", "03-6441-2324", "Akasaka Enokizaka Building 5F, 1-7-1 Akasaka, Minato-ku, Tokyo"),
    SAPPORO("Sapporo office", "011-213-1001", "6th floor of the Continental Building, Minami 1-jo Nishi 11-1, Chuo-ku, Sapporo"),
    MIYAGI("Miyagi Business Center", "022-352-7581", "2F, 3-6-1 Chuo, Tagajo City, Miyagi Prefecture");

    private String addressName;
    private String addressPhone;
    private String address;

    OfficeAddress(String addressName, String addressPhone, String address) {
        this.addressName = addressName;
        this.addressPhone = addressPhone;
        this.address = address;
    }

    public String getAddressName() {
        return addressName;
    }

    public String getAddressPhone() {
        return addressPhone;
    }

    public String getAddress() {
        return address;
    }
}
