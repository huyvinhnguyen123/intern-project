package time.keeping.urbanvn.entity.postition;

import java.util.List;
import java.util.Optional;

public interface PositionService {
    List<Position> getPositions();
    Optional<Position> getPosition(Integer positionId);
    Position addPosition(Position position, OfficePosition officePosition);
    List<Position> addPositions(Position position, OfficePosition officePosition);
}
