package time.keeping.urbanvn.entity.address.init;

public enum OfficeWard {
    NONE(""),
    WARD1("Ward 1"),
    WARD2("ward 2"),
    WARD3("ward 3");

    private String wardName;

    OfficeWard(String wardName) {
        this.wardName = wardName;
    }

    public String getWardName() {
        return wardName;
    }
}
