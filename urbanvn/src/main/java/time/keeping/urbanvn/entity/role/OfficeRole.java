package time.keeping.urbanvn.entity.role;

public enum OfficeRole {
    USER("user", "User only sees and timekeeping yourself"),
    ADMIN("admin", "Admin has full access");

    private String roleName;
    private String roleDescription;

    OfficeRole(String roleName, String roleDescription) {
        this.roleName = roleName;
        this.roleDescription = roleDescription;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }
}
