package time.keeping.urbanvn.entity.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

// import java.util.ArrayList;
// import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, String>{

    @Query(value = "SELECT * FROM account c WHERE c.email = ?1", nativeQuery = true)
    Optional<Account> findByEmail(String email);
}
