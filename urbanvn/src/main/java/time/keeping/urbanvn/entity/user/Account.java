package time.keeping.urbanvn.entity.user;

import java.util.UUID;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import time.keeping.urbanvn.entity.profile.Profile;
import time.keeping.urbanvn.entity.role.Role;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
@Entity
@Table(name = "account")
public class Account {
    @Id
    @Column(name = "user_id", length = 255, updatable = false)
    private String userId;

    @Column(name = "username", length = 50, unique = true)
    @NotBlank(message = "username cannot be blank")
    private String username;

    @Column(name = "email", length = 255, unique = true)
    @NotBlank(message = "email cannot be blank")
    private String email;
    
    @Column(name = "password", length = 255, nullable = false)
    @NotBlank(message = "password cannot be blank")
    private String password;

    @Transient
    private String retypePassword;

    @Column(name = "created_at", updatable = false)
    private String createdAt;

    @Column(name = "updated_at")
    private String updatedAt;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private Set<Role> roles = new HashSet<>();

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
	private Profile profile;

    public Account() {
        this.userId = UUID.randomUUID().toString();
        this.createdAt = CurrentDate.getCurrentDate();
        this.updatedAt = CurrentDate.getCurrentDate();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Account other = (Account) obj;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }
}
