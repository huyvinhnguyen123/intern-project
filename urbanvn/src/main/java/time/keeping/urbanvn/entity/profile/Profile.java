package time.keeping.urbanvn.entity.profile;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import time.keeping.urbanvn.entity.address.Address;
import time.keeping.urbanvn.entity.payroll.PayRoll;
import time.keeping.urbanvn.entity.postition.Position;
import time.keeping.urbanvn.entity.timecard.TimeCard;
import time.keeping.urbanvn.entity.user.Account;
import time.keeping.urbanvn.utils.CurrentDate;

@Data
@Entity
@Table(name = "profile")
public class Profile {
    @OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "user_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Account account;

    @ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
	name="profile_position",
	joinColumns=@JoinColumn(name="profile_id"),
	inverseJoinColumns=@JoinColumn(name="position_id")
	)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private List<Position> positions;

    @ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
	name="profile_address",
	joinColumns=@JoinColumn(name="profile_id"),
	inverseJoinColumns=@JoinColumn(name="address_id")
	)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private List<Address> addresses;

    @Id
    @Column(name = "profile_id", updatable = false) 
    private String profileId;

    @Column(name = "avatar_url", length = 255, nullable = false)
    private String avatarUrl;

    @NotBlank(message = "phone cannot be blank")
    @Column(name = "phone_number", length = 11, nullable = false)
    private String phoneNumber;

    @Column(name = "hire_date", updatable = false)
    private String hireDate;

    @Column(name = "created_at", updatable = false)
	private String createdAt;
	
	@Column(name = "updated_at")
	private String updatedAt;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TimeCard> timeCards;

    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PayRoll> payRolls;

    public Profile() {
        this.profileId = UUID.randomUUID().toString();
        this.hireDate = CurrentDate.getCurrentDateWithoutHMS();
        this.createdAt = CurrentDate.getCurrentDate();
        this.updatedAt = CurrentDate.getCurrentDate();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((profileId == null) ? 0 : profileId.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Profile other = (Profile) obj;
        if (profileId == null) {
            if (other.profileId != null)
                return false;
        } else if (!profileId.equals(other.profileId))
            return false;
        return true;
    }
}
