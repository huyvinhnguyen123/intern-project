package time.keeping.urbanvn.entity.user;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService{
    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(username).orElse(null);
        if(account == null){
            throw new UsernameNotFoundException("User not found with gmail: " + username);
        }
        return User.builder()
        .username(account.getEmail())
        .password(account.getPassword())
        .roles("USER")
        .build();
    }
}
