package time.keeping.urbanvn.entity.address;

import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Integer> {}
