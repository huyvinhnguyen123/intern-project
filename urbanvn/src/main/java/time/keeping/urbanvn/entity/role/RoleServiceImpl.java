package time.keeping.urbanvn.entity.role;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import time.keeping.urbanvn.entity.user.Account;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Override
    public List<Role> getRoles() {
        log.info("Get roles success!");
        return (List<Role>) roleRepository.findAll();
    }

    @Override
    public Role addRole(Account account, OfficeRole role) {
        Role newRole = new Role();
        if(account != null){
            newRole.setAccount(account);
            newRole.setRoleName(role.getRoleName());
            newRole.setRoleDescription(role.getRoleDescription());
            roleRepository.save(newRole);
            log.info("Add role success!");
            return newRole;
        }else{
            log.error("Not found user!");
            return null;
        }
    }

    @Override
    public List<Role> addRoles(Account user, OfficeRole role){
        List<Role> roles = new ArrayList<Role>();
        roles.add(addRole(user, role));
        return roles;
    }
}
