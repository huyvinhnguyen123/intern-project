package time.keeping.urbanvn.entity.address;

import org.springframework.data.repository.CrudRepository;

public interface DistrictRepository extends CrudRepository<District, Integer> {}
