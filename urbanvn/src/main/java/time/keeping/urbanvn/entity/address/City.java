package time.keeping.urbanvn.entity.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;


import lombok.Data;

@Data
@Entity
@Table(name = "address")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private Integer cityId;

    @NotBlank(message = "city cannot be blank")
    @Column(name = "city_name", length = 100, nullable = false)
    private String cityName;
}
