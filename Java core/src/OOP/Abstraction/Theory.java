package OOP.Abstraction;

/*
^ Abstraction(OOP)

* Abstraction aims to hide complexity from users and show them only relevant information. 
* For example, if you’re driving a car, you don’t need to know about its internal workings.
* The same is true of Java classes. You can hide internal implementation details using abstract classes or interfaces. 
* On the abstract level, you only need to define the method signatures (name and parameter list) 
* and let each class implement them in their own way.

* Java provides two ways to implement abstraction: abstract classes and interfaces. 
* With abstract classes, you can achieve partial abstraction, while interfaces make total (100%) abstraction possible.

* Abstraction in Java:
* Hides the underlying complexity of data
* Helps avoid repetitive code
* Presents only the signature of internal functionality
* Gives flexibility to programmers to change the implementation of abstract behavior
* Partial abstraction (0-100%) can be achieved with abstract classes
* Total abstraction (100%) can be achieved with interfaces
*/
//================================================================================================================================== 
/*
 & Example: How abstraction works in practice
 
 * An abstract class is a superclass (parent class) that cannot be instantiated. 
 * To create a new object, you need to instantiate one of its child classes. 
 * Abstract classes can have both abstract and non abstract methods. 
 * Abstract methods contain only the method signature, while non abstract methods declare the method body as well. 
 * Abstract classes are defined with the abstract keyword.
 
 ? In the code example below, we create an abstract class called Animal with two abstract and one non abstract method.
 */

abstract  class  Animal  {
    // abstract methods
    abstract  void  move();
    abstract  void  eat();
    // non abstract method (Phuong thuc khong truu tuong)
    void  label()  {
    System.out.println("Animal's data:");
    }
}

/*
 ? Then, we extend it with two child classes: Bird and Fish. 
 ? Both of them define their own implementations of the move() and eat() abstract methods.
 */

class  Bird  extends Animal {
    void  move()  {
        System.out.println("Moves by flying.");
    }
    void  eat()  {
        System.out.println("Eats birdfood.");
    }
}

class  Fish  extends Animal {
    void  move()  {
        System.out.println("Moves by swimming.");
    }
    void  eat()  {
        System.out.println("Eats seafood.");
    }
}

/*
 ? Now, we’ll test it with the help of the TestBird and TestFish classes. 
 ? Both initialize an object (myBird and myFish) and call the one concrete (label()) and the two abstract (move() and eat()) methods.
 */

// main class
class  TestBird  {
    public  static  void  main(String[] args)  {
        Animal myBird =  new Bird();
        myBird.label();
        myBird.move();
        myBird.eat();
    }
}

// main class
class  TestFish  {
    public  static  void  main(String[] args)  {
        Animal myFish =  new Fish();
        myFish.label();
        myFish.move();
        myFish.eat();
    }
}
//================================================================================================================================== 
/*
 & Interfaces

 * An interface is a 100% abstract class. 
 * It can only have static, final, and public fields and abstract methods. 
 * It’s frequently referred to as a blueprint of a class as well. 
 * Java interfaces allow you to implement multiple inheritances in your code, as a class can implement any number of interfaces. 
 * Classes can access an interface with the implements keyword.
 
 ? In the example, we define two interfaces: Animal with two abstract methods (interface methods are abstract by default) 
 ? and Bird with two static fields and an abstract method.
 */

interface  interfaceAnimal  {
    public  void  eat();
    public  void  sound();
}


interface  interfaceBird  {
    int numberOfLegs = 2;
    String outerCovering =  "feather";
    public  void  fly();
}

/*
 ? The class Eagle implements both interfaces. It defines its own functionality for the three abstract methods. 
 ? The eat() and sound() methods come from the Animal class, while fly() comes from Bird.
 */

class  Eagle  implements interfaceAnimal, interfaceBird {
    public  void  eat()  {
        System.out.println("Eats reptiles and amphibians.");
    }
    public  void  sound()  {
        System.out.println("Has a high-pitched whistling sound.");
    }
    public  void  fly()  {
        System.out.println("Flies up to 10,000 feet.");
    }
}

/*
 ? In the TestEagleInterfaces test class, we instantiate a new Eagle object (called myEagle) and print out all the fields and methods to the console.

 ? As static fields (numberOfLegs and outerCovering) don’t belong to a specific object but to the interface, 
 ? we need to access them from the Bird interface instead of the myEagle object.
 */

class  TestEagleInterfaces  {
    public  static  void  main(String[] args)  {
        Eagle myEagle =  new Eagle();
        
        myEagle.eat();
        myEagle.sound();
        myEagle.fly();
        
        System.out.println("Number of legs: "  + interfaceBird.numberOfLegs);
        System.out.println("Outer covering: "  + interfaceBird.outerCovering);
    }
}
//================================================================================================================================== 
  