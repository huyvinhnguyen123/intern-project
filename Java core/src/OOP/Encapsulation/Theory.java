package OOP.Encapsulation;

/*
^ Encapsulation(OOP)

* Encapsulation helps with data security, allowing you to protect the data stored in a class from system-wide access. 
* As the name suggests, it safeguards the internal contents of a class like a capsule.

* You can implement encapsulation in Java by making the fields (class variables) private and accessing them via their public getter and setter methods. 
* JavaBeans are examples of fully encapsulated classes.

* Encapsulation in Java:
* Restricts direct access to data members (fields) of a class
* Fields are set to private
* Each field has a getter and setter method
* Getter methods return the field
* Setter methods let us change the value of the field
*/
//================================================================================================================================== 
/*
 & Example: how encapsulation works in practice
 
 ? With encapsulation, you can protect the fields of a class. 
 ? To do so, you need to declare the fields as private and provide access to them with getter and setter methods.

 ? The Animal class below is fully encapsulated. 
 ? It has three private fields, and each has its own pair of getter and setter methods.
 */

class  Animal  {
    private String name;
    private  double averageWeight;
    private  int numberOfLegs;
    
    // Getter methods
    public String getName()  {
        return name;
    }
    public  double  getAverageWeight()  {
        return averageWeight;
    }
    public  int  getNumberOfLegs()  {
        return numberOfLegs;
    }
    // Setter methods
    public  void  setName(String name)  {
        this.name  = name;
    }
    public  void  setAverageWeight(double averageWeight)  {
        this.averageWeight  = averageWeight;
    }
    public  void  setNumberOfLegs(int numberOfLegs)  {
        this.numberOfLegs  = numberOfLegs;
    }
}

/*
 ? The TestAnimal class first creates a new Animal object (called myAnimal), 
 ? then defines a value for each field with the setter methods, and finally prints out the values using the getter methods.
 */

class  TestAnimal  {
    public  static  void  main(String[] args)  {
        Animal myAnimal =  new Animal();
        myAnimal.setName("Eagle");
        myAnimal.setAverageWeight(1.5);
        myAnimal.setNumberOfLegs(2);
        System.out.println("Name: "  + myAnimal.getName());
        System.out.println("Average weight: "  + myAnimal.getAverageWeight()  +  "kg");
        System.out.println("Number of legs: "  + myAnimal.getNumberOfLegs());
    }
}