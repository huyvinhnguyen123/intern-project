package OOP.Inheritance;

/*
^ Inheritance(OOP)

* Inheritance makes it possible to create a child class that inherits the fields and methods of the parent class. 
* The child class can override the values and methods of the parent class, but it’s not necessary. 
* It can also add new data and functionality to its parent.

* Parent classes are also called superclasses or base classes, 
* while child classes are known as subclasses or derived classes as well. 
* Java uses the extends keyword to implement the principle of inheritance in code.

* Inheritance in Java:
* A class (child class) can extend another class (parent class) by inheriting its features
* Implements the DRY (Don’t Repeat Yourself) programming principle
* Improves code reusability
* Multi-level inheritance is allowed in Java (a child class can have its own child class as well)
* Multiple inheritances are not allowed in Java (a class can’t extend more than one class)
*/
//================================================================================================================================== 
/*
 & Example: How inheritance works in practice

 ? Inheritance lets you extend a class with one or more child classes that inherit the fields and methods of the parent class. 
 ? It’s an excellent way to achieve code reusability. In Java, you need to use the extends keyword to create a child class.

 ? In the example below, the Eagle class extends the Bird parent class. 
 ? It inherits all of its fields and methods, plus defines two extra fields that belong only to Eagle.
 */

class  Bird  {
    public String reproduction =  "egg";
    public String outerCovering =  "feather";
    public  void  flyUp()  {
        System.out.println("Flying up...");
    }
    public  void  flyDown()  {
        System.out.println("Flying down...");
    }
}

class  Eagle  extends Bird {
    public String name =  "eagle";
    public  int lifespan = 15;
}

/*
 ? The TestEagleInheritance class instantiates a new Eagle object (called myEagle) 
 ? and prints out all the information (both the inherited fields and methods and the two fields defined by the Eagle class).
 */

 class  TestEagleInheritance  {
    public  static  void  main(String[] args)  {
        Eagle myEagle =  new Eagle();
        System.out.println("Name: "  + myEagle.name);  System.out.println("Reproduction: "  + myEagle.reproduction);
        System.out.println("Outer covering: "  + myEagle.outerCovering);
        System.out.println("Lifespan: "  + myEagle.lifespan);
        myEagle.flyUp();
        myEagle.flyDown();
    }
}
//==================================================================================================================================