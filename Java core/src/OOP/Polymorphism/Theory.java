package OOP.Polymorphism;

/*
^ Polymorphism(OOP)

* Polymorphism refers to the ability to perform a certain action in different ways. 
* In Java, polymorphism can take two forms: method overloading and method overriding.

* Method overloading happens when various methods with the same name are present in a class. 
* When they are called, they are differentiated by the number, order, or types of their parameters. 
* Method overriding occurs when a child class overrides a method of its parent.

* Polymorphism in Java:
* The same method name is used several times
* Different methods of the same name can be called from an object
* All Java objects can be considered polymorphic (at the minimum, they are of their own type and instances of the Object class)
* Static polymorphism in Java is implemented by method overloading
* Dynamic polymorphism in Java is implemented by method overriding
*/
//================================================================================================================================== 
/*
 & Example: How polymorphism works in practice
 ? Polymorphism makes it possible to use the same code structure in different forms. 
 ? In Java, this means that you can declare several methods with the same name as long as they are different in certain characteristics.

 ? As mentioned above, Java provides two ways to implement polymorphism: method overloading and method overriding.
 */

/*
 & Static polymorphism (method overloading)

 ? Method overloading means that you can have several methods with the same name within a class. 
 ? However, the number, names, or types of their parameters need to be different.

 ? For example, the Bird() class below has three fly() methods. 
 ? The first one doesn’t have any parameters, the second one has one parameter (height), and the third one has two parameters (name and height).
 */

class  Bird  {
    public  void  fly()  {
        System.out.println("The bird is flying.");
    }
    public  void  fly(int height)  {
        System.out.println("The bird is flying "  + height +  " feet high.");
    }
    public  void  fly(String name,  int height)  {
        System.out.println("The "  + name +  " is flying "  + height +  " feet high.");
    }
}

/*
 ? The test class instantiates a new Bird object and calls the fly() method three times: first, without parameters, second, with one integer parameter for height, and third, with two parameters for name and height.
 */

class  TestBirdStatic  {
    public  static  void  main(String[] args)  {
        Bird myBird =  new Bird();
        myBird.fly();
        myBird.fly(10000);
        myBird.fly("eagle", 10000);
    }
}
//==================================================================================================================================
/*
 & Dynamic polymorphism (method overriding)

 ? Using the method overriding feature of Java, you can override the methods of a parent class from its child class.

 ? In the code example below, the Bird class extends the Animal class. Both have an eat() method. 
 ? By default, Bird inherits its parent’s eat() method. 
 ? However, as it also defines its own eat() method, Java will override the original method and call eat() from the child class.
 */

class  Animal  {
    public  void  eat()  {
        System.out.println("This animal eats insects.");
    }
}

class  Bird2  extends Animal {
    public  void  eat()  {
        System.out.println("This bird eats seeds.");
    }
}

/*
 ? The TestBirdDynamic class first instantiates a new Animal object and calls its eat() method. 
 ? Then, it also creates a Bird object and calls the polymorphic eat() method again.
 */

class  TestBirdDynamic  {
    public  static  void  main(String[] args)  {
        Animal myAnimal =  new Animal();
        myAnimal.eat();
        Bird2 myBird =  new Bird2();
        myBird.eat();
    }
}

/*
 * Abstraction, encapsulation, polymorphism, and inheritance are the four main theoretical principles of object-oriented programming.
 */
//==================================================================================================================================