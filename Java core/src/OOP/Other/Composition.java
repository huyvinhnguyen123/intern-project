package OOP.Other;

/*
 ^ Composition

 * Composition is a stricter form of aggregation. 
 * It occurs when the two classes you associate are mutually dependent and can’t exist without each other.
 * For example, take a Car and an Engine class. 
 * A Car cannot run without an Engine, while an Engine also can’t function without being built into a Car. 
 * This kind of relationship between objects is also called a PART-OF relationship.
 
 * Composition in Java:
 * A restricted form of aggregation
 * Represents a PART-OF relationship between two classes
 * Both classes are dependent on each other
 * If one class ceases to exist, the other can’t survive alone

 * Summary
 * OOP concepts in Java help you to structure your program more efficiently. 
 * The seven object-oriented principles we’ve explored here 
 * (abstraction, encapsulation, polymorphism, inheritance, association, aggregation, and composition) can help you reuse your code, 
 * prevent security issues, and improve the performance of your Java applications.
 */