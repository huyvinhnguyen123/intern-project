package OOPDemo;

import java.util.UUID;

public class User extends Account {
    private String id;
    private String role;
    private String description;

    public User(String id, String role, String description) {
        this.id = id;
        this.role = role;
        this.description = description;
    }

    public User() {
        this.id = UUID.randomUUID().toString();
        this.role="User";
        this.description = "User role is read only";
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
