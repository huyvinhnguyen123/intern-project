package OOPDemo;

import java.util.UUID;

public class Admin extends Account{
    private String id;
    private String role;
    private String description;

    public Admin(String id, String role, String description) {
        this.id = id;
        this.role = role;
        this.description = description;
    }

    public Admin() {
        this.id = UUID.randomUUID().toString();
        this.role="Admin";
        this.description = "Admin role can have full access";
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
