package OOPDemo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class CurrentDate {
    public static String getCurrentDateWithoutHMS () {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = dateFormat.format(date);
		System.out.println(currentDate);
		return currentDate;
	}
}
