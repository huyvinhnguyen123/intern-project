package OOPDemo;

public class Main {
    public static void main(String[] args){
        Account newAccount = new Account();
        newAccount.setEmail("huy.vinhnguyen@outlook.com");
        newAccount.setPassword("123");
        newAccount.setUsername("Huy");

        System.out.println("===============Account.java=================");
        System.out.println("Id: " + newAccount.getAccountId());
        System.out.println("Email: " + newAccount.getEmail());
        System.out.println("Password: " + newAccount.getPassword());
        System.out.println("Username: " + newAccount.getUsername());
        System.out.println("CreatedAt: " + newAccount.getCreatedAt());
        System.out.println("===============Account.java=================");

        User roleUser = new User();
        roleUser.setEmail("huy.vinhnguyen@outlook.com");

        System.out.println("===============User.java=================");
        System.out.println("Id: " + roleUser.getId());
        System.out.println("Role: " + roleUser.getRole());
        System.out.println("Desc: " + roleUser.getDescription());
        System.out.println("Email: " + roleUser.getEmail());
        System.out.println("===============User.java=================");

    }
}
