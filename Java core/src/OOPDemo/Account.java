package OOPDemo;

import java.util.UUID;

public class Account {
    private String accountId;
    private String username;
    private String email;
    private String password;
    private String createdAt;

    public Account() {
        this.accountId = UUID.randomUUID().toString();
        this.createdAt = CurrentDate.getCurrentDateWithoutHMS();
    }

    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String id) {
        this.accountId = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    
}
