package Questions;

/* 
 ^ Java core question
*/
//==================================================================================================================================
/*
 ^ 1. What is JDK, JRE and JVM?

 ? JDK(Java Development Kit)
 * JDK stands for Java Development Kit and contains tools for developing, compiling, and debugging Java programs. 
 * It includes the JRE, as well as the Java compiler (javac), debugger (jdb), and other tools.
 
 ? JRE(Java Runtime Environment)
 * JRE stands for Java Runtime Environment and provides the runtime environment for Java programs to run. 
 * It includes the JVM, as well as the Java class libraries and other supporting files.

 ? JVM(Java Virtual Machine)
 * JVM stands for Java Virtual Machine and is the runtime environment in which Java bytecode is executed. 
 * It is responsible for interpreting the bytecode and translating it into machine code that can be executed by the computer's processor.

 ^ Addition Compare between Java ME, Java SE & Java EE
 * Overall Java ME, Java SE & Java EE is 3 difference version is built from Java.
 
 ~ Java SE(Java Platform, Standard Edition):
 * Also known as Java Core, this is the standard and basic version of Java that serves as the foundation for other versions.
 * Contains general APIs (like java.lang, java.util...) and many other special APIs.
 * Includes all the basic features and features of the Java language such as variables, primitive data types, Arrays, Streams, Strings, Java Database Connectivity (JDBC)...
 * The most famous feature of Java is that the JVM is also built for this version only.
 * Java SE is used primarily to create applications for the Desktop environment.
 
 ~ Java ME(Java Platform, Micro Edition):
 * This is the version used for creating applications that run on embedded systems such as mobile devices and small devices.
 * Devices using Java ME often have limitations such as processing capacity limits, power (battery) limitations, small display screens, etc.
 * Java ME also supports the use of web compression technology, which reduces network usage and improves low-cost internet access.
 * Java ME uses many of Java SE's libraries and APIs, and many of its own libraries and APIs.
  
 ~ Java EE(Java Platform, Enterprise Edition):
 * This is the Enterprise version of Java, used for developing web applications.
 * Java EE contains Enterprise APIs like JMS, EJB, JSPs/Servlets, JNDI
 * Java EE uses many components of Java SE and adds its own features such as Servlets, JavaBeans, etc.
 * Java EE uses HTML, CSS, JavaScript… to create websites and web services.
 * Many other languages ​​are also used to develop web applications like Java EE (.Net, PHP..) but Java EE is used a lot for its functionality, security, portability, etc. 
*/
//==================================================================================================================================
/* 
 ^ 2. Main method in java

 & public static void main(String args[])
 ~ public: 
 * This means that the method can be accessed from anywhere, including outside the class. 
 * Since main is the entry point of a Java program, it needs to be accessible from outside the class.
 
 ~ static: 
 * This means that the method belongs to the class and not to any instance of the class. 
 * Since the main method is the entry point of a Java program, it needs to be available without creating an instance of the class.
 
 ~ void: 
 * This means that the method does not return any value. 
 * The main method is the starting point of a Java program, and it doesn't need to return any value.
  
 ^ Why you must create 
 & public static void main(String args[])
 ^ instead of
 & public void main(String args[])

 * If you remove the static keyword, then you would need to create an instance of the class to call the main method, 
 * which is not possible because the main method is the starting point of the program and no instance of the class has been created yet.
*/
//==================================================================================================================================
/* 
 ^ 3. Why is java platform independent?

 * Because java bytecode can run on any operating system
*/
//==================================================================================================================================
/* 
 ^ 4. Why is java not 100% object oriented?

 * Because java uses 8 primitive data types: byte, int, long, float, double, char, short, boolean
*/
//==================================================================================================================================
/* 
 ^ 5. What is wrapper class in java?

 * In Java, a wrapper class is a class that allows primitive data types to be accessed as objects. 
 * Wrapper classes provide a way to use primitive data types (int, boolean, char, etc.) in a more object-oriented manner.

 * Each primitive data type has a corresponding wrapper class. 
 * The wrapper class for int is Integer, the wrapper class for boolean is Boolean, the wrapper class for char is Character, and so on.

 * Wrapper classes are often used when dealing with collections, as collections can only hold objects, not primitive types. 
 * They are also used in Java's reflection API, which allows Java code to examine and modify the behavior of Java programs at runtime.
*/
//==================================================================================================================================
/* 
 ^ 6. Compare ArrayList & Vector?

 ? ArrayList:
    * Not synchronized
    * High speed
    * Default increment size is 50%
    * Increment size can be specified
    * Use Iterator for browsing

 ? Vector:
    * Synchronized
    * Slower speed than ArrayList
    * Default increment size doubles the size of the array
    * Increment size can be specified
    * Use Enumeration or Iterator for browsing
*/
//==================================================================================================================================
/* 
  ^ 7. Compare Array & ArrayList

 ? Array(static array):
    * Fixed size
    * Only store 1 data type
    * Can store primitive data types and objects
    * Fast storage and operation speed
                                 
 ? ArrayList(dynamic array):
    * Size change
    * Can store many types of data
    * Only store object data type
    * Slow storage and operation speed
*/
//==================================================================================================================================
/* 
 ^ 8. What is final in java?

 * final is used to limit user manipulation. It can be used in many different contexts such as:
 * Final variable: To create a variable with a constant value (Constant)
 * Final method: Prevent method overriding
 * Final class: Prevent inheritance
*/
//==================================================================================================================================
/* 
 ^ 9. What is static in java?

 * In Java, static is a keyword that is used to create variables, methods, 
 * and classes that can be accessed without creating an instance of the class. 
 * When a variable or method is declared as static, it becomes associated with the class rather than with individual objects of the class. 
 * This means that static variables and methods can be accessed using the class name, rather than through an object of the class.
  
 * Static variables and methods can be useful when you want to maintain a single copy of a variable or method across all instances of a class, 
 * or when you want to provide a utility method that does not require any instance-specific state.

 * Static classes are classes that contain only static methods or static variables, and cannot be instantiated.

 * Overall, static is a powerful keyword in Java that can simplify code and provide useful functionality. 
 * However, it should be used carefully and sparingly, as overuse of static can make code harder to understand and maintain.
*/

/* 
 & Static variable
*/
class Example {
   static int count = 0; // static variable
   
   public void increment() {
      count++; // accessing static variable inside a non-static method
   }

   public static void main(String[] args) {
      Example obj1 = new Example();
      Example obj2 = new Example();

      obj1.increment();
      obj2.increment();

      System.out.println("Obj1: count is " + Example.count); // Output: Obj1: count is 2
      System.out.println("Obj2: count is " + Example.count); // Output: Obj2: count is 2
   }
}

/* 
 & Static method
*/
class Example2 {
   static void myStaticMethod() {
      System.out.println("This is a static method");
   }

   public void myPublicMethod() {
      System.out.println("This is a public method");
   }

   public static void main(String[] args) {
      Example2.myStaticMethod(); // calling static method

      Example2 obj = new Example2();
      obj.myPublicMethod(); // calling non-static method using object of class
   }
}

/* 
 & Static block
*/
class Example3 {
   static {
       System.out.println("This is a static block");
   }

   public static void main(String[] args) {
       // main method
   }
}

/* 
 & Static inner class:
*/
class Example4 {
   static class Inner {
       void display() {
           System.out.println("This is a static inner class");
       }
   }

   public static void main(String[] args) {
       Example4.Inner obj = new Example4.Inner();
       obj.display(); // creating object of static inner class
   }
}
//==================================================================================================================================
/* 
 ^ 10. In java, user can define multiple classes in single source file?

 * In Java, you can define multiple classes in a single source file, 
 * but only one of them can be declared as public. 
 * The name of the source file must match the name of the public class defined in the file. 
 * This is a requirement of the Java compiler, which uses the public class as the entry point for the program.

 * If you do not declare a class as public, you can have multiple classes in the same source file with any name. 
 * These classes can only be accessed within the same package and cannot be accessed outside the package. 
 * This is known as package-private access.
*/