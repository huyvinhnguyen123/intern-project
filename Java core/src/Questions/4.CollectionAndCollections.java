package Questions;

import java.util.Collections;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
 ^ Collection & Collections question
*/
//==================================================================================================================================
/*
 ^ 1. Compare List, Set, Map

 ? List:
 * Ordered collection of elements
 * Can contain duplicates
 * Allows access to elements by index
 * Examples: ArrayList, LinkedList
  
 ? Set:
 * Unordered collection of elements
 * Does not allow duplicates
 * Does not provide access to elements by index
 * Examples: HashSet, LinkedHashSet, TreeSet
  
 ? Map:
 * Key-value mapping
 * Keys must be unique
 * Values can be duplicated
 * Examples: HashMap, LinkedHashMap, TreeMap

 * In general, you would use a List when you need to maintain order and may have duplicate elements, 
 * a Set when you don't care about order and want to ensure uniqueness of elements, 
 * and a Map when you need to associate values with keys. 
*/
//==================================================================================================================================
/* 
 ^ 2. Compare ArrayList & LinkedList 

 * ArrayList and LinkedList are both implementations of the List interface in Java, 
 * but they differ in their underlying data structures and their performance characteristics.
 
 ~ Implementation: 
 * ArrayList is implemented as an array, while LinkedList is implemented as a doubly-linked list. 
 * This means that ArrayList provides faster random access to elements, 
 * while LinkedList is better for adding or removing elements from the middle of the list.
 
 ~ Performance:
 * ArrayList provides O(1) time complexity for getting an element at a specific index, 
 * while LinkedList provides O(n) time complexity for the same operation. 
 * However, LinkedList provides O(1) time complexity for adding or removing an element from the beginning or end of the list, 
 * while ArrayList has O(n) time complexity for those operations.
 
 ~ Memory usage:
 * ArrayList uses contiguous memory to store its elements, 
 * so it requires a block of memory that is large enough to hold all of the elements in the list.
 * In contrast, LinkedList stores each element in a separate node, 
 * which can be more memory-efficient when dealing with large lists 
 * or when elements are frequently added or removed from the middle of the list.
 
 ~ Usage:
 * Iterator performance: Iterating over the elements of an ArrayList using an Iterator is generally faster than iterating over a LinkedList, 
 * since the elements in an ArrayList are stored in contiguous memory. 
 * However, iterating over a LinkedList using a ListIterator provides faster access to elements 
 * when iterating backwards or forwards in the list.
 
 * In summary, ArrayList is faster when it comes to accessing elements by index, 
 * while LinkedList is faster when it comes to insertions and deletions. 
 * The choice between the two depends on the requirements of your specific application. 
*/

/* 
 ^ Here are some examples to demonstrate the differences between ArrayList and LinkedList in Java.
*/

/* 
 & Example 1: Adding and removing elements
*/
class Example1 {
    public static void main(String[] args){
        long startTime = System.nanoTime();

        // LinkedList example
        List<Integer> linkedList = new LinkedList<>();

        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(1, 4);

        linkedList.remove(0);
        linkedList.remove(Integer.valueOf(3));

        long endTime = System.nanoTime();

        System.out.println("Time taken for LinkedList: " + (endTime - startTime) + " nanoseconds");

    

        startTime = System.nanoTime();

        // ArrayList example
        List<Integer> arrayList = new ArrayList<>();

        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(1, 4);

        arrayList.remove(0);
        arrayList.remove(Integer.valueOf(3));

        endTime = System.nanoTime();

        System.out.println("Time taken for ArrayList: " + (endTime - startTime) + " nanoseconds");

    }
}

/* 
 * The main difference between the two is in the performance of adding and removing elements. 
 * ArrayList uses an array to store its elements, so when we add or remove an element from the middle of the list, 
 * it has to shift all the subsequent elements to make room for the new element or fill in the gap left by the removed element. 
 * This can be an expensive operation, especially for large lists.

 * LinkedList, on the other hand, uses a doubly-linked list to store its elements, 
 * so adding or removing an element from the middle of the list involves simply updating a few pointers 
 * to link the surrounding nodes to the new or removed node. 
 * This makes adding and removing elements from the middle of the list faster than in ArrayList.
*/

/* 
 & Example 2: Iterating over elements
*/
// ArrayList example
class ArrayListExample2{;
    public static void main(String[] args){
        long startTime = System.nanoTime();
        List<Integer> arrayList = new ArrayList<>();

        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);

        for (Integer element : arrayList) {
            System.out.println(element);
        }
        long endTime = System.nanoTime();
        System.out.println("Time taken for ArrayList: " + (endTime - startTime) + " nanoseconds");
    }
}

// LinkedList example
class LinkedListExample2{
    public static void main(String[] args){
        long startTime = System.nanoTime();
        List<Integer> linkedList = new LinkedList<>();

        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        for (Integer element : linkedList) {
            System.out.println(element);
        }
        long endTime = System.nanoTime();
        System.out.println("Time taken for LinkedList: " + (endTime - startTime) + " nanoseconds");
    }
}

/* 
 * The performance of iterating over the elements of an ArrayList and a LinkedList is generally similar, 
 * since both provide O(n) time complexity for iterating over all the elements in the list. 
 * However, as I mentioned earlier, iterating over the elements of an ArrayList using an Iterator is generally faster 
 * than iterating over a LinkedList, since the elements in an ArrayList are stored in contiguous memory.
*/



//==================================================================================================================================
/* 
 ^ 3. Compare Map & LinkedHashSet

 * Both HashSet and LinkedHashSet are implementations of the Set interface in Java and store a collection of unique elements. 
 * The main differences between them are: 
 
 ~ Ordering: 
 * HashSet does not maintain any order of elements while LinkedHashSet maintains the insertion order of elements.
 ~ Performance: 
 * HashSet provides better performance in terms of adding, removing, and checking the existence of elements than LinkedHashSet.
 ~ Memory usage: 
 * LinkedHashSet uses slightly more memory than HashSet as it needs to maintain the order of elements..

 * In general, if the order of elements is important, or if you need to iterate through the elements in the order they were added, use LinkedHashSet. 
 * Otherwise, use HashSet for better performance and memory usage.
*/
//==================================================================================================================================
/* 
 ^ 4. Difference Between Array.asList() & List.of()

 * Arrays.asList() and List.of() are two methods used to create a list in Java. However, they differ in some important aspects:
 
 ~ 1. Return type:
 * Arrays.asList() returns a List object, which is backed by the original array. 
 * This means that any changes made to the list will be reflected in the original array and vice versa.
 * List.of() returns an immutable list. Any attempt to modify the list will result in an UnsupportedOperationException.

 ~ 2. Elements:
 * Arrays.asList() can take an array or a sequence of elements as arguments, which are then added to the list.
 * List.of() takes a sequence of elements as arguments and returns an immutable list
 
 ~ 3. Null elements:
 * Arrays.asList() allows null elements to be added to the list.
 * List.of() does not allow null elements.
 
 ~ 4. Resizing:
 * Arrays.asList() allows the list to be resized. For example, you can add or remove elements from the list.
 * List.of() does not allow the list to be resized. Once created, the list cannot be modified.
 
 * In summary, Arrays.asList() is used to create a mutable list backed by an array, 
 * whereas List.of() is used to create an immutable list containing a sequence of elements.
*/
//==================================================================================================================================
/* 
 ^ 5. Compare HashMap & LinkedHashMap

 * HashMap and LinkedHashMap are two popular implementations of the Map interface in Java. 
 * The main difference between HashMap and LinkedHashMap lies in their order and iteration performance:
 
 ? HashMap: 
 * It does not maintain the order of elements in the map. 
 * The order in which the elements are inserted is not guaranteed, and hence iteration order is not guaranteed as well. 
 * HashMap provides O(1) complexity for inserting, deleting, and retrieving elements from the map.
 
 ? LinkedHashMap: 
 * It maintains the order of elements in the map. 
 * The order of elements is maintained in which they are inserted into the map. 
 * It provides O(1) complexity for inserting, deleting, and retrieving elements from the map. 
 * However, the performance of iteration is slightly slower than HashMap due to the extra overhead of maintaining the order of elements.
 
 * In summary, if you don't care about the order of elements and need better performance, HashMap is a better choice. 
 * But if you need to maintain the order of elements and performance is not a concern, LinkedHashMap is a better choice.
*/
//==================================================================================================================================
/* 
 ^ 6. Singleton in Collections

 * In Java, the term "singleton" typically refers to a design pattern known as the Singleton Pattern. 
 * It is a creational design pattern that restricts the instantiation of a class to a single instance 
 * and provides a global point of access to that instance.
 
 * In the context of collections, the term "singleton" does not have a specific meaning. 
 * However, there is a class called Collections in Java which provides utility methods for working with collections (e.g., lists, sets, maps). 
 * The Collections class does not have a method called "singleton". 
 * It does, however, have a method called singletonList() which returns an immutable list containing only the specified element. 
 * This is a way to create a list with a single element.

 * Here's an example of how you can use Collections.singletonList() to create a singleton list:
*/

class SingleTonExample {
    public static void main(String[] args) {
        String element = "Singleton Element";
        List<String> singletonList = Collections.singletonList(element);

        System.out.println(singletonList); // Output: [Singleton Element]
        
        // Attempting to modify the singleton list will result in an UnsupportedOperationException
        // singletonList.add("New Element"); // Throws UnsupportedOperationException
    }
}