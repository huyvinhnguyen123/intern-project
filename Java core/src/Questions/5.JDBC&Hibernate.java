package Questions;

/* 
 ^ 1. Compare 4 types JDBC driver

 ~ JDBC-ODBC Bridge Driver:
 * This driver uses the ODBC (Open Database Connectivity) driver to connect to the database. 
 * It is not recommended for production use since it has poor performance and is no longer actively developed.
 
 & Example 
 * **********************************************************************************************************
 ? Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
 ? Connection conn = DriverManager.getConnection("jdbc:odbc:database_name", "username", "password");
 * **********************************************************************************************************

 ~ Native Driver:
 * This driver is specific to the database and is written in the same language as the database. 
 * It provides a direct connection to the database and is faster than the JDBC-ODBC bridge driver.
 
 & Example 
 * **********************************************************************************************************
 ? Class.forName("com.mysql.jdbc.Driver");
 ? Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database_name", "username", "password");
 * **********************************************************************************************************

 ~ Network Protocal Driver
 * This driver uses middleware to connect to the database over a network. 
 * It requires some additional setup and configuration but is faster and more reliable than the JDBC-ODBC bridge driver.
  
 & Example 
 * **********************************************************************************************************
 ? Class.forName("oracle.jdbc.driver.OracleDriver");
 ? Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@hostname:1521:database_name", "username", "password");
 * ********************************************************************************************************** 

 ~ Thin Driver
 * This driver is similar to the network protocol driver but uses a pure Java implementation of the middleware, 
 * making it platform-independent. It is the most popular and recommended driver for production use.
 
 & Example 
 * **********************************************************************************************************
 ? Class.forName("org.postgresql.Driver");
 ? Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/database_name", "username", "password");
 * ********************************************************************************************************** 
*/

/* 
 ^ 2. Compare PostgreSQL & MySQL

 * PostgreSQL and MySQL are both popular open-source relational database management systems. 
 * Here are some of the key differences between the two:
 
 ~ Data types: 
 * PostgreSQL supports a wider range of data types than MySQL, including arrays, user-defined types, 
 * and geospatial data types.
 
 ~ SQL Compliance: 
 * PostgreSQL is more SQL-compliant than MySQL, which means that it adheres more closely to SQL standards. 
 * MySQL has its own syntax and features that are not part of the standard SQL language.
 
 ~ ACID compliance: 
 * Both PostgreSQL and MySQL are ACID-compliant, but PostgreSQL is considered to have stronger ACID compliance 
 * and is often preferred for applications that require high levels of consistency and reliability.
 
 ~ Performance: 
 * Both databases have good performance, but the specific performance characteristics may depend on the workload 
 * and usage patterns. In general, MySQL is often considered to be faster for simple queries, 
 * while PostgreSQL may be faster for more complex queries.
 
 ~ Licensing: 
 * PostgreSQL is released under the PostgreSQL License, while MySQL is released under the GNU General Public License (GPL). 
 * This means that the two databases have different licensing requirements and restrictions.
 
 ~ Features: 
 * MySQL has traditionally been favored for its ease of use and speed, 
 * while PostgreSQL has been favored for its advanced features, 
 * such as support for stored procedures, triggers, and advanced indexing.
*/

/* 
 ^ 3. In hibernate what difference between save, persist, update, merge, saveOrUpdate?

 * save(): This method saves a new object to the database. If the object is already persistent, it throws an exception.
 
 * persist(): This method also saves a new object to the database, 
 * but it guarantees that the object will be assigned an identifier immediately. 
 * If the object is already persistent, it does nothing.
 
 * update(): This method updates a persistent object with new values. 
 * It throws an exception if the object is not already persistent.
 
 * merge(): This method updates a detached object with new values and returns the updated persistent object. 
 * If the object is not already persistent, it will be saved as a new object.
 
 * saveOrUpdate(): This method saves a new object or updates an existing one, 
 * depending on whether the object is already persistent or not.
 
 ? Here are some differences between these methods:

 * save() and persist() both save new objects, 
 * but persist() is usually preferred when the identifier is generated by the database.
 
 * update() updates a persistent object, whereas merge() updates a detached object and returns a new persistent object.
 
 * saveOrUpdate() can save or update an object, depending on whether it is already persistent or not. 
  
 * In general, it is recommended to use save() or persist() for new objects and update() or merge() for updating existing objects. 
 * The choice between update() and merge() depends on the use case, as each has its own advantages and disadvantages. 
 * saveOrUpdate() can be used when it is unclear whether the object is new or existing.
*/
