package Questions;

/* 
 ^ General question
*/
//==================================================================================================================================
/* 
 ^ Question 1: What does the basic website include?

 * To build a website and bring it to users, we need the following basic components:

 ? Domain name (domain): 
 * is the address of that website on the Internet. 
 * In order for users to access the content of the website, 
 * they need to type the correct website address into the address bar of the web browser.
 
 ? Hosting: 
 * a server running on the Internet that stores your data and runs your application.
 
 ? Source code: 
 * is the source code of the web application. When users visit the website, 
 * the web application will return the content to display for user interaction, and update the stored data if necessary.
*/
//==================================================================================================================================
/* 
 ^ Question 2: Work of a Web Developer

 * Web Developer will usually be divided into 2 different directions:

 ? FrontEnd Developer: 
 * build the user-side display interface (client-side). 
 * Usually will use HTML, CSS and JavaScript for designing and building applications that handle direct interaction with users.
 * Backend Developer: build applications that run on the server-side (server-side).
 
 ? Backend Developer: 
 * build applications that run on the server-side (server-side). 
 * Backend's job is to handle receiving requests from the client, perform logic, update data into the database if needed, 
 * and return a response to the client.
 
 ? Source code: 
 * is the source code of the web application. When users visit the website, 
 * the web application will return the content to display for user interaction, and update the stored data if necessary.
*/
//==================================================================================================================================
/* 
 ^ Question 3: What is DOM, HTML DOM?

 * DOM - short for Document Object Model or document object model, 
 * is an application programming interface (API) used to access HTML and XML documents. 
 * DOM has a tree data structure (Tree), containing nodes (Node), 
 * each node will have its own properties for displaying values ​​or accessing data.
  
 * The HTML DOM is the standard object model and programming interface for HTML, which defines:

 * HTML elements are objects
 * Properties and events for all HTML elements
 * Methods to access HTML elements
 * The HTML DOM helps you perform actions on web pages, change the content displayed, and get results back from the action.
*/
//==================================================================================================================================
/* 
 ^ Question 4: What is CORS?

 * CORS or Cross-Origin Resource Sharing is a browser mechanism that allows access 
 * and control of resources located outside of a certain domain. 
 * CORS was born because the same-origin policy related to security is installed into all browsers today. 
 * Specifically, a website cannot access and use information and resources of users on other websites without permission.
  
 * With a typical Web application, developers will often query other domains via API calls, 
 * and to conform to the same-origin policy, CORS is used to notify the browser by the application running. 
 * on this domain are allowed to access resources in the specified domain.
*/
//==================================================================================================================================
/* 
 ^ Question 5: What is RESTfull API?

 * An API (Application Programming Interface) or application programming interface 
 * is a software intermediary that allows two applications to communicate with each other. 
 * Usually in a Web application, the API is used to communicate between the frontend client application and the backend server application. 
 * RESTfull API is a standard used in the design of APIs for Web applications, 
 * it focuses on the management of system resources (files, images, audio, video, dynamic data, ...) transmitted over HTTP.

 * RESTfull API is designed to operate mainly on CRUD (Create, Read, Update, Delete) method equivalent to 4 HTTP protocols, POST, GET, PUT and DELETE.
*/
//==================================================================================================================================
/* 
 ^ Question 6: How many methods are there in HTTP request?

 * There are 9 types in all, which include:
 
 * GET: used to get information from the server
 * HEAD: same as GET but response only returns header, no body
 * POST: send information to the server through HTTP parameters
 * PUT: overwrites all information of the object with what is submitted
 * PATCH: overwrite the changed information of the object
 * DELETE: delete resources on the server
 * CONNECT: establish connection to server
 * OPTIONS: describe communication options for resources
 * TRACE: perform a loop-back test to the resource
*/
//==================================================================================================================================
/* 
 ^ Question 7: What is Database? What is a database management system?

 * A database or database is an organized collection of structured information or data, usually stored online in a computer system.
 * A database management system (DBMS) is a comprehensive database software program that serves as an interface between databases 
 * and end users or programs to allow users to retrieve , updating and managing information is organized and optimized.
  
 * Some common types of databases:

 * Relational database: organized as a collection of columns and rows
 * Object-oriented databases: represent information in the form of objects
 * Distributed databases: organize files located in different computer systems and networks
 * NoSQL databases: storing and manipulating unstructured data
*/
//==================================================================================================================================
/* 
 ^ Question 8: Distinguish Cookies, Session, Session Storage and Local Storage

 * Cookie is a piece of written information created and stored on the browser of the user's machine 
 * (client) usually created when the user visits a website, it will remember information such as login name, password. 
 * password or user-selected options included. 
 
 * Session (session) is a way to save the data of the user using the website in the form of a file on the server, 
 * it contains information about the user and the session to help authenticate the account information. 
 * Working. At the end of the session (close the browser), the session information will be lost.
 
 * Local Storage:
 * Indefinite storage: Means only deleted with JavaScript, or cleared of browser memory, or deleted using localStorage API.
 * Store 5MB: Local Storage allows you to store relatively large information up to 5MB, storing the largest amount of information in the 3 types.
 * Do not send information to the server such as Cookies for better security
 
 * Session Storage:
 * Store on Client: Like localStorage, sessionStorage is also used to store data on the visitor's browser (client).
 * Data loss on tab closure: SessionStorage data is lost when you close the browser.
 * Data not sent to Server
 * More information stored than cookies (at least 5MB) 
*/
//==================================================================================================================================
/* 
 ^ Question 9: What is SEO? What is a standard SEO website?

 * SEO - Search Engine Optimization is a concept that only optimizes the search position for the website on popular search engines such as Google, Facebook, etc. 
 * For example, when users search for a keyword related to the industry, product or business. 
 * products that your website offers because the search results showing your website on the top is a way to attract users, 
 * and also be appreciated by search engines in priority ranking. 
 * An SEO-standard website is a website designed to be compatible with the search engine (search engine) of Google or Facebook, etc. 
 * Each search engine has a different search algorithm, so there are also different SEO methods dedicated to it. 
 * However, to evaluate an SEO standard website, when programming a website, 
 * there are some specific techniques such as attaching meta tags, optimizing layout with CSS, placing deep links, etc.
*/