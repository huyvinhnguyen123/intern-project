package Questions;

/* 
 ^ OOP question
*/
//==================================================================================================================================
/* 
 ^ 1. How many Properties in OOP?

 ~ Encapsulation: 
 ~ Inheritance: 
 ~ Polymorphism: 
 ~ Abstraction: 
*/
//==================================================================================================================================
/* 
 ^ 2. What is Encapsulation in OOP?

 * Encapsulation in Java is a mechanism of wrapping data and code together into a single unit, called a class. 
 * It allows us to hide the implementation details of an object from the outside world and only expose a public interface that can be used to interact with the object.
*/

class BankAccount {
    private String accountNumber;
    private double balance;

    public BankAccount(String accountNumber, double balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount) {
        if (amount > balance) {
            throw new IllegalArgumentException("Insufficient funds");
        }
        balance -= amount;
    }
}

/* 
 * In this example, the BankAccount class has two private data fields (accountNumber and balance) 
 * that can only be accessed by the public methods of the class (getAccountNumber(), getBalance(), deposit(), and withdraw()). 
 * This means that the internal workings of the BankAccount class are hidden from the outside world, 
 * and the only way to interact with a BankAccount object is through its public interface. This is an example of encapsulation in action.
*/
//==================================================================================================================================
/* 
 ^ 3. What is Inheritance in OOP?

 * Inheritance is a mechanism in Java that allows a new class to be based on an existing class. 
 * The new class, called the subclass, can inherit the properties and methods of the existing class, 
 * called the superclass, and can also add new properties and methods of its own.
*/

class Animal {
    protected String name;
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void eat() {
        System.out.println(name + " is eating");
    }
}

class Cat extends Animal {
    public void meow() {
        System.out.println(name + " is meowing");
    }
}

class Main {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setName("Fluffy");
        cat.eat();
        cat.meow();
    }
}

/* 
 * In this example, the Cat class is a subclass of the Animal class. 
 * The Cat class inherits the name property and the eat() method from the Animal class, and it adds a new method called meow(). 
 * The Main class creates a new Cat object, sets its name to "Fluffy", and calls its eat() and meow() methods.
*/
//==================================================================================================================================
/* 
 ^ 4. What is Polymorphism in OOP?

 * Polymorphism in Java refers to the ability of objects to take multiple forms or have multiple behaviors based on the context in which they are used. 
*/

class Animal1 {
    public void makeSound() {
        System.out.println("Unknown animal sound");
    }
}

class Cat1 extends Animal1 {
    @Override
    public void makeSound() {
        System.out.println("Meow");
    }
}

class Dog1 extends Animal1 {
    @Override
    public void makeSound() {
        System.out.println("Woof");
    }
}

class PolymorphismExample {
    public static void main(String[] args) {
        Animal1 myPet = new Cat1();
        myPet.makeSound(); // prints "Meow"
        
        myPet = new Dog1();
        myPet.makeSound(); // prints "Woof"
    }
}
//==================================================================================================================================
/* 
 ^ 4.1. How many types of polymorphism?

 * There are 2 types of polymorphism: compile-time polymorphism and runtime polymorphism.
 
 ? Compile-time polymorphism: 
 * Compile-time polymorphism is achieved using method overloading, where multiple methods have the same name but different parameters.
 ? Runtime polymorphism:
 * Runtime polymorphism is achieved using method overriding, 
 * where a subclass provides a different implementation of a method that is already defined in its superclass. 
 * The method in the subclass must have the same name, return type, and parameters as the method in the superclass.
  
 ! Note: Can't overriding private or static method. 
 ! If you create a method with the same return type and same parameters in the subclass, it will hide the method of the parent class
*/
//==================================================================================================================================
/* 
 ^ 5. What is Abstraction in OOP?

 * Abstraction is a mechanism of showing only necessary details to the user and hiding the implementation details. 
 * In Java, abstraction can be achieved through abstract classes and interfaces.
*/

abstract class Shape {
    public abstract void draw();
}

class Rectangle extends Shape {
    public void draw() {
        System.out.println("Drawing Rectangle");
    }
}

class Circle extends Shape {
    public void draw() {
        System.out.println("Drawing Circle");
    }
}

class MainAbstract {
    public static void main(String[] args) {
        Shape rect = new Rectangle();
        rect.draw();
        
        Shape circle = new Circle();
        circle.draw();
    }
}
//==================================================================================================================================
/* 
 ^ 5.1. What difference between interface and abstract

 ~ Implementation: 
 * Abstract classes can have both abstract and non-abstract methods, 
 * while interfaces can only have abstract methods (prior to Java 8). With Java 8, 
 * interfaces can also have default and static methods.
 
 ~ Multiple inheritance: 
 * Java allows a class to inherit from only one abstract class, but it can implement multiple interfaces.
 
 ~ Access Modifiers: 
 * Abstract classes can have public, protected, and private access modifiers for their members, 
 * whereas interface methods are implicitly public and cannot be private or protected.
 
 ~ Variables: 
 * Abstract classes can have instance variables, whereas interfaces cannot.
 
 ~ Constructors: 
 * Abstract classes can have constructors, while interfaces cannot.
*/
//==================================================================================================================================
/* 
 ^ 6. Types of Inheritance in Java

 * Java support 4 types of Inheritance:
 ~ Single Inheritance: 
 * A subclass is derived from single base class
 ~ Multilevel inheritance: 
 * A derived class is created from another derived class.
 ~ Hierarchical Inheritance: 
 * Multiple derived classes are created from a single base class.
 ~ Multiple inheritance (not supported in Java): 
 * A subclass is derived from more than one base class.
 ~ Hybrid inheritance (not directly supported in Java): 
 * It is a combination of multiple and multilevel inheritance.
*/

/* 
 ~ Single Inheritance:
*/
class Animal6 {
    public void eat() {
        System.out.println("Animal is eating");
    }
}

class Dog6 extends Animal6 {
    public void bark() {
        System.out.println("Dog is barking");
    }
}

class Main6 {
    public static void main(String[] args) {
        // Usage
        Dog6 dog = new Dog6();
        dog.eat(); // Animal is eating
        dog.bark(); // Dog is barking
    }
}

/* 
 ~ Multilevel inheritance: 
*/
class Animal61 {
    public void eat() {
        System.out.println("Animal is eating");
    }
}

class Dog61 extends Animal61 {
    public void bark() {
        System.out.println("Dog is barking");
    }
}

class Bulldog61 extends Dog61 {
    public void bite() {
        System.out.println("Bulldog is biting");
    }
}

class Main61 {
    public static void main(String[] args) {
        // Usage
        Bulldog61 bulldog = new Bulldog61();
        bulldog.eat(); // Animal is eating
        bulldog.bark(); // Dog is barking
        bulldog.bite(); // Bulldog is biting

    }
}

/* 
 ~ Hierarchical Inheritance: 
*/
class Animal62 {
    public void eat() {
        System.out.println("Animal is eating");
    }
}

class Dog62 extends Animal62 {
    public void bark() {
        System.out.println("Dog is barking");
    }
}

class Cat62 extends Animal62 {
    public void meow() {
        System.out.println("Cat is meowing");
    }
}

class Main62 {
    public static void main(String[] args) {
        // Usage
        Dog62 dog = new Dog62();
        dog.eat(); // Animal is eating
        dog.bark(); // Dog is barking

        Cat62 cat = new Cat62();
        cat.eat(); // Animal is eating
        cat.meow(); // Cat is meowing

    }
}

/* 
 ~ Multiple inheritance (not supported in Java): 
*/
interface Animal63 {
    void eat();
}

interface Vehicle63 {
    void move();
}

class Dog63 implements Animal63, Vehicle63 {
    public void eat() {
        System.out.println("Dog is eating");
    }

    public void move() {
        System.out.println("Dog is moving");
    }

    public void bark() {
        System.out.println("Dog is barking");
    }
}

class Main63 {
    public static void main(String[] args) {
        // Usage
        Dog63 dog = new Dog63();
        dog.eat(); // Dog is eating
        dog.move(); // Dog is moving
        dog.bark(); // Dog is barking

    }
}

/* 
 ~ Hybrid inheritance (not directly supported in Java): 
*/
class Animal64 {
    public void eat() {
        System.out.println("Animal is eating");
    }
}

interface Vehicle64 {
    void move();
}

class Dog64 extends Animal64 implements Vehicle64 {
    public void bark() {
        System.out.println("Dog is barking");
    }

    public void move() {
        System.out.println("Dog is moving");
    }
}

class Bulldog64 extends Dog64 {
    public void bite() {
        System.out.println("Bulldog is biting");
    }
}

class Main64 {
    public static void main(String[] args) {
        // Usage
        Bulldog64 bulldog = new Bulldog64();
        bulldog.eat(); // Animal is eating
        bulldog.bark(); // Dog is barking
        bulldog.move(); // Dog is moving
        bulldog.bite(); // Bulldog is biting

    }
}
//==================================================================================================================================