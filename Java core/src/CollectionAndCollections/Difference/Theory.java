package CollectionAndCollections.Difference;

import java.util.HashMap;
import java.util.Map;

/*
 ^ Difference between collection and collections

 * In Java, Collection and Collections are two different things:
 
 * Collection is an interface that represents a group of objects, often called a container. 
 * It defines common methods that all collections should implement, such as add, remove, size, contains, and so on. 
 * Collection is part of the java.util package and is implemented by classes like List, Set, and Queue.
 
 * Collections is a utility class in the java.util package that provides static methods to operate on objects of type Collection, 
 * such as sort, shuffle, reverse, min, max, binarySearch, and so on. 
 * These methods take a collection as input and return a modified or new collection as output. 
 * For example, the Collections.sort() method can be used to sort a list of objects.
*/

/* 
 ^ Collection in java

 * In Java, a collection is a group of objects that are stored and managed together. 
 * It is an interface that represents a group of elements, such as an array or a list, 
 * and provides a set of operations for working with those elements. 
 * The Collection interface is a part of the Java Collections Framework, 
 * which provides a set of classes and interfaces for working with collections.

 * The Java Collections Framework includes a variety of collection types, including:
 * List: an ordered collection of elements that allows duplicates
 * Set: an unordered collection of elements that does not allow duplicates
 * Map: a collection of key-value pairs, where each key is unique
 * Queue: a collection that orders its elements in a specific way
 * Each of these collection types has its own specific implementation in Java, such as ArrayList for List, 
 * HashSet for Set, HashMap for Map, and PriorityQueue for Queue.

 * Collections are commonly used in Java to store, retrieve, and manipulate data, 
 * making it easier to manage and process large amounts of data. 
 * They provide a flexible and efficient way to work with groups of objects in Java.
*/

/* 
 & Example map collection
*/

class Example{
    public static void main(String[] args){
        // Create a new HashMap object
        Map<String, Integer> myMap = new HashMap<>();
        // Add key-value pairs to the map
        myMap.put("apple", 1);
        myMap.put("banana", 2);
        myMap.put("orange", 3);

        // Retrieve a value from the map using its key
        int value = myMap.get("banana");
        System.out.println(value);

        // Check if the map contains a specific key(search)
        boolean containsKey = myMap.containsKey("apple");
        System.out.println(containsKey);

        // Remove a key-value pair from the map
        myMap.remove("orange");
        System.out.println(myMap);
    }
}