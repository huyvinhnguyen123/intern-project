package CollectionAndCollections.Collection;

/*
 ^ Collection in Java
 
 * In Java, the Collection framework provides a set of interfaces and classes to store and manipulate groups of objects. 
 * The Collection framework provides various types of interfaces such as List, Set, Queue, and Map that can be used to implement different types of collections based on the specific requirements.

 * The List interface provides an ordered collection of elements, 
 * which can be accessed by their index. The ArrayList and LinkedList classes are examples of classes that implement the List interface.

 * The Set interface provides an unordered collection of elements, which does not allow duplicates. 
 * The HashSet, LinkedHashSet, and TreeSet classes are examples of classes that implement the Set interface.

 * The Queue interface provides a collection of elements in which the elements are stored and accessed in a FIFO (first-in, first-out) manner. 
 * The LinkedList and PriorityQueue classes are examples of classes that implement the Queue interface.

 * The Map interface provides a collection of key-value pairs, in which each key is associated with a value. 
 * The HashMap, TreeMap, and LinkedHashMap classes are examples of classes that implement the Map interface.
*/
//==================================================================================================================================
/* 
 & Here's an example of using a Collection in Java: 
*/
import java.util.ArrayList;
import java.util.Collection;

class ExampleCollection {
    public static void main(String[] args) {
        // create a new collection of integers
        Collection<Integer> numbers = new ArrayList<>();

        // add some numbers to the collection
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);

        // print out the size of the collection
        System.out.println("Size of collection: " + numbers.size());

        // remove a number from the collection
        numbers.remove(2);

        // print out the updated size of the collection
        System.out.println("Size of collection after removing 2: " + numbers.size());
    }
}