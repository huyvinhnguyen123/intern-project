package CollectionAndCollections.Collections;

/* 
 ^ Collections in Java

 * Collections in Java refer to a group of interfaces and classes that are used to manage and manipulate groups of objects, 
 * or collections, in a more organized and efficient manner. 
 * It provides numerous utility methods for data manipulation, searching, and sorting of collections.

 * Some commonly used interfaces and classes in Java Collections include:

 * List: An ordered collection that allows duplicates.
 * Set: A collection that does not allow duplicates.
 * Map: A collection of key-value pairs, where each key is unique.
 * ArrayList: An implementation of List that is backed by an array.
 * LinkedList: An implementation of List that is backed by a linked list.
 * HashSet: An implementation of Set that uses a hash table for storage.
 * HashMap: An implementation of Map that uses a hash table for storage.
 * TreeSet: An implementation of Set that is sorted in natural order.
 * TreeMap: An implementation of Map that is sorted by the keys.
*/

/*
 & Here's an example of using some of the methods provided by the Collections class:
 */
import java.util.*;

class CollectionsExample {
      public static void main(String[] args) {

        // create a list of integers
        List<Integer> numbers = new ArrayList<>();
        numbers.add(4);
        numbers.add(1);
        numbers.add(7);
        numbers.add(3);

        // sort the list in ascending order
        Collections.sort(numbers);
        System.out.println(numbers); // output: [1, 3, 4, 7]

        // shuffle the list
        Collections.shuffle(numbers);
        System.out.println(numbers); // output: [4, 1, 7, 3]

        // find the minimum value in the list
        int min = Collections.min(numbers);
        System.out.println(min); // output: 1

        // find the maximum value in the list
        int max = Collections.max(numbers);
        System.out.println(max); // output: 7

        // create an empty set of strings
        Set<String> names = new HashSet<>();

        // add some names to the set
        names.add("Alice");
        names.add("Bob");
        names.add("Charlie");
        names.add("Alice");

        // remove duplicates from the set
        Set<String> uniqueNames = new HashSet<>(names);
        System.out.println(uniqueNames); // output: [Charlie, Bob, Alice]
      }
  }
