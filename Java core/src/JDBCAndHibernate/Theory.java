package JDBCAndHibernate;

/* 
 ^ What is JDBC?

 * JDBC stands for Java Database Connectivity. 
 * JDBC is a Java API to connect and execute the query with the database. 
 * It is a part of JavaSE (Java Standard Edition). 
 * JDBC API uses JDBC drivers to connect with the database. 
 * There are four types of JDBC drivers:
  
 * JDBC-ODBC Bridge Driver,
 * Native Driver,
 * Network Protocol Driver,
 * Thin Driver
 
 * We can use JDBC API to access tabular data stored in any relational database. 
 * By the help of JDBC API, we can save, update, delete and fetch data from the database. 
 * It is like Open Database Connectivity (ODBC) provided by Microsoft.
*/

/* 
 ^ What is Hibernate?

 * Hibernate is an open-source Object-Relational Mapping (ORM) framework 
 * that simplifies the process of mapping an object-oriented domain model to a traditional relational database. 
 * It provides a set of APIs for performing CRUD (Create, Read, Update, Delete) operations on the database without requiring developers to write SQL queries.
 
 * Hibernate maps Java classes to database tables, 
 * and its primary feature is to automatically generate the SQL statements 
 * needed to manipulate the database tables based on the object-oriented domain model. 
 * Hibernate provides a layer of abstraction between the application and the database, 
 * which makes it easier to switch between different databases or database vendors 
 * without having to change the application code.
 
 * Hibernate also provides features such as caching, lazy loading, and automatic transaction management, 
 * which can improve the performance of the application and reduce the amount of code required to interact with the database.
*/



