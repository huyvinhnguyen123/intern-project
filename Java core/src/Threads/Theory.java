package Threads;

/*
^ What is threads?

* In Java, a thread is a unit of execution within a program. 
* Threads allow a program to perform multiple tasks simultaneously or in parallel. 
* Each thread can be thought of as a separate flow of execution within a program.

* When a Java program starts, a single thread is created by the JVM (Java Virtual Machine) to execute the main() method of the program. 
* Additional threads can be created by the program using the Thread class provided by Java.

* Threads are scheduled by the operating system to run on the CPU. 
* The operating system gives each thread a certain amount of time on the CPU, and then switches to another thread. 
* This is known as context switching. The time allocated to each thread is determined by the operating system's scheduling algorithm.

* Threads can communicate with each other and synchronize their operations using various constructs such as locks, semaphores, and monitors. 
* This allows threads to work together to accomplish a task or to share resources such as memory.

* Threads can communicate with each other and synchronize their operations using various constructs such as locks, semaphores, and monitors. 
* This allows threads to work together to accomplish a task or to share resources such as memory.
*/
/*
* When creating programs that use threads, there are several important concepts that you should be familiar with:

? 1. Thread synchronization: 
* When multiple threads access shared resources such as variables or objects, 
* it is important to synchronize their access to avoid race conditions and data inconsistencies

? 2. Thread communication:
* Threads may need to communicate with each other in order to coordinate their activities. 
* This can be done using wait/notify mechanisms, or by using higher-level constructs such as blocking queues or semaphores.

? 3. Thread safety:
* It is important to ensure that code executed by multiple threads is thread-safe, 
* meaning that it can be executed correctly and without conflicts in a multi-threaded environment.

? 4. Deadlocks: 
* Deadlocks occur when two or more threads are blocked, waiting for each other to release resources. 
* It is important to design programs in a way that avoids deadlocks.

? 5. Thread pools:
* Thread pools can be used to manage the lifecycle of threads, 
* reducing the overhead of creating and destroying threads and making it easier to manage resources.

? 6. Thread interruption:
* Threads can be interrupted by other threads, and it is important to handle thread interruption properly to ensure that programs behave correctly 
* and don't leave resources in an inconsistent state.

* By understanding these concepts, you can write programs that effectively use threads to improve performance and responsiveness, 
* while ensuring that they are reliable and free from race conditions and other issues that can arise in multi-threaded programs.
*/