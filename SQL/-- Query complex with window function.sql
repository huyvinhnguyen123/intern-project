-- Pratice SQL with complex queries using window function

-- Query 1:

-- Write a SQL query to fetch all the duplicate records from users table (output one record)

select * from users

-- using ctid
select u.user_id, u.user_name, u.email
from users u
where u.ctid not in (
	select min(ctid) as ctid
	from users
	group by user_name
	order by ctid
)

-- ================================================
-- Explain this query
-- ================================================
-- We're using this query for fetching all the duplicate records from the user's table
-- In the table, we have the duplicate records at rows 4 and 5 which have the same user_name, same email
-- 1. So we will select all columns (or specify column...your's choice) from inside the other select
-- 2. We will use ctid. ctid is a system column that represents the physical location of a row in a table. 
-- It stands for "tuple ID". The ctid value is a combination of the block number and the index of the row within that block.
-- 2.1. In the provided query, ctid is used as a means to identify duplicate records. 
-- The subquery (SELECT MIN(ctid) AS ctid FROM users GROUP BY user_name ORDER BY ctid) finds the minimum ctid for each user_name, effectively selecting the "first" row for each group.
-- 3. The outer query then selects all rows from the users table where the ctid is not in the set of minimum ctid values, effectively excluding the "first" rows and retrieving only the duplicate records.

-- using window function
select user_id, user_name, email
from (
select *,
row_number() over (partition by user_name order by user_id) as rn
from users u
) x
where x.rn <> 1;

-- ================================================
-- Explain this window function query
-- ================================================
-- We're using this query for fetching all the duplicate records from the user's table
-- In the table, we have the duplicate records at rows 4 and 5 which have the same user_name, same email
-- 1. So we will select all columns (or specify column...your's choice) from inside the other select
-- 2. In the other select we will select all columns and ROW_NUMBER() using "OVER" 
-- to assign a unique row number to each row in the result set, based on the specified PARTITION and ordering.
-- 2.1. ROW_NUMBER(): It is a window function that assigns a unique sequential number to each row within a partition of the result set.
-- 2.2. OVER (PARTITION BY user_name ORDER BY user_id): The OVER clause defines the window within which the ROW_NUMBER() function operates.
-- that means this is the syntax for the ROW_NUMBER(). 
-- ================================================
-- 		ROW_NUMBER() OVER (
--    	[PARTITION BY partition_expression]
--    	ORDER BY sort_expression [ASC | DESC]
--		)
-- ================================================
-- 2.3. PARTITION BY: This clause is optional and allows you to partition the result set into groups based on one or more columns. 
-- 2.4. ORDER BY: This clause specifies the column(s) by which the rows are ordered within each partition. 
-- The row numbers will be assigned based on this ordering.
-- 2.5. By using ROW_NUMBER() with PARTITION BY and ORDER BY, the query generates a unique row number for each row within each user_name partition, based on the ascending order of user_id. 
-- This means that rows with the same user_name will have consecutive row numbers starting from 1.
-- 3. x.rn <> 1: This clause is mean that row_number will be different from the same, which means it will get the duplicate record but will return only one record of them
-- Like I said  we have duplicate records at rows 4 and 5 then it will get only one
-- If i change to "=" it's mean they will get all records in table if these records is unique except duplicate records

----------------------------------------------------------------------------------------
-- Query 2:

-- Write a SQL query to fetch the second last record from a employee table.

select * from employee

-- using limit & offset
select emp_id, emp_name, dept_name, salary
from employee
order by emp_id desc
limit 1 offset 1

-- ================================================
-- Explain this query
-- ================================================
-- We're using this query to fetch the second last record from an employee table.
-- To do that we use limit & offset here how it works
-- 1. Select all columns (or specify column...your's choice) from the employee table
-- 2. We will order by emp_id from biggest to smallest
-- 3. We will use pagination by using offset and only get one record on one page
-- Here I will get the second last record on page 1 (page starting from 0) => page 0 we have the last record

-- using window function
select emp_id, emp_name, dept_name, salary
from(
	select *,
	row_number() over (order by emp_id desc) as rn
	from employee
) x
where x.rn = 2;

-- ================================================
-- Explain this window function query
-- ================================================
-- We're using this query to fetch the second last record from an employee table.
-- 1. Select all columns (or specify column...your's choice) from inside the other select
-- 2. In the other select we will select all columns and ROW_NUMBER() using "OVER" 
-- this section was explained at first query. Here I will explain briefly.
-- Inside the other select we will select a unique row number for each row based on the descending order of user_id.
-- This means the last row will have consecutive row numbers starting from 1.
-- Then the second last row will be 2
-- 3. where x.rn = 2: we will get the second last record

----------------------------------------------------------------------------------------
-- Query 3:

-- Write a SQL query to display only the details of employees who either earn the highest salary
-- or the lowest salary in each department from the employee table.

select * from employee

-- using select in select and or
select *,
(select max (salary)
from employee 
where dept_name = e.dept_name ),
(select min (salary)
from employee 
where dept_name = e.dept_name )
from employee e
where e.salary = (
	select max(salary) as max_salary
	from employee
	where dept_name = e.dept_name
	group by dept_name
) or e.salary = (
	select min(salary) as min_salary
	from employee
	where dept_name = e.dept_name
	group by dept_name
)
order by dept_name

-- ================================================
-- Explain this query
-- ================================================
-- We're using this query to display only the details of employees who either earn the highest salary
-- or the lowest salary in each department from the employee table.
-- 1. Select all columns (or specify column...your's choice), (select max(salary) from employee) to get the max column and the same for min column from employee
-- 2. We will compare salary with max using equal in (select max(salary) from employee and we will group by dept_name & the dept_name from outer select is the same with inside select)
-- 3. We will continue to compare salary with min using or and using equal. The same with above
-- 4. We will order by dept_name 

-- using window function
select x.*
from employee e
join (
	select *,
	min(salary) over (partition by dept_name) as min_salary,
	max(salary) over (partition by dept_name) as max_salary
	from employee
) x
on e.emp_id = x.emp_id
and (e.salary = x.min_salary or e.salary = x.max_salary)
order by x.dept_name, x.salary;

-- ================================================
-- Explain this window function query
-- ================================================
-- We're using this query to display only the details of employees who either earn the highest salary
-- or the lowest salary in each department from the employee table.
-- 1. Select all columns (or specify column...your's choice), here we will use x.* from employee
-- Explain: In the query SELECT x.*, x.* is a shorthand notation that represents all columns in the table or subquery referenced by the alias x. 
-- It allows you to select all the columns without explicitly listing them one by one.
-- 2. We will join with another query inside and assign it to variable x
-- 2.1. Inside this query, we will select all columns and unique min, max from the partition by dept_name from employee
-- 3. After that, we will check & make sure that e.emp_id is equal with x.emp_id
-- 4. And e.salary us equal x.min_salary or equal x.max_salary
-- 5. Finally, we will order by query inside x.dept_name & x.salary

----------------------------------------------------------------------------------------
-- Query 4:

-- From the doctors table, fetch the details of doctors who work in the same hospital but in different speciality.

select * from doctors;

-- using select query
select d1.name, d1.speciality, d1.hospital
from doctors d1
join doctors d2
on d1.hospital = d2.hospital
and d1.speciality <> d2.speciality

-- ================================================
-- Explain this query
-- ================================================
-- We're using this query to fetch the details of doctors who work in the same hospital but in different speciality from the doctors table
-- 1. Select specify column d1.name, d1.speciality, d1.hospital from doctors d1
-- 2. After that we will join again with doctors but with different value d2
